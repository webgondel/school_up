import expressJwt from 'express-jwt';
import config from './config';

const jwt = () => {
  return expressJwt({ secret: config.jwtSecret }).unless({
    path: [
      // public routes that don't require authentication
      '/api/auth/login',
      '/api/auth/signup',
      /^\/api\/main\/getFilesByFolderId\/.*/,
      /^\/api\/main\/getCloudByCloudId\/.*/,
      /^\/api\/main\/getSecurityAnswerByUserId\/.*/,
      /^\/api\/upload\/.*/,
      /^\/api\/user\/getUserByUsername\/.*/,
      '/api/user/resetPasswordById',
    ]
  });
};

export default jwt;
