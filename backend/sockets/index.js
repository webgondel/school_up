import {mainChatSocket} from "./mainChatSocket";

export const defineSocket = (io) => {
    mainChatSocket(io);
};
