const chatRoomPrefix = "rs9dx2d_";
import connection from '../config/database';
const sessionsMap = {};


export const mainChatSocket = (io) => {
    io.on('connection', (socket) => {
        console.log('new socket connected...');

        socket.on('setChatRoom', (user_id) => {
            sessionsMap[user_id] = socket.id;
        });

        socket.on('newMessage', (messageData) => {
            if (messageData.is_group_chat === 0) {
                console.log('private chatting >>>>>>>>>>>', messageData.receiver_user_id, sessionsMap[messageData.receiver_user_id]);
                io.to(sessionsMap[messageData.receiver_user_id]).emit('newMessage', messageData);
            } else {
                console.log('group chatting...');
                connection.query(`SELECT * FROM users WHERE class ='${messageData.class_name}' AND active=1`, function (error, results, fields) {
                    if (error) throw error;
                    for (const classUser of results) {
                        if (classUser.user_id === messageData.sender_user_id) {
                            continue;
                        }
                        io.to(sessionsMap[classUser.user_id]).emit('newMessage', messageData);
                    }
                });
            }

        });
    });
};
