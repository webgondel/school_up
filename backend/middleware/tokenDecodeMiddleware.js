import jwt from 'jsonwebtoken';
import config from '../config/config';

const tokenDecodeMiddleware = async (req, res, next) => {
    const authorization = req.headers["authorization"];
    if (authorization) {
        const token = req.headers["authorization"].replace(/^Bearer\s/, '');
        jwt.verify(token, config.jwtSecret, (err, user) => {
            if(err) {
                res.status(401).send({
                    message: 'Token is invalid.'
                });
            }
            req.userToken = user;
        });
    }
    next();
};

export default tokenDecodeMiddleware;
