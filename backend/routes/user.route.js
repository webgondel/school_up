import { Router } from 'express';
import * as UserController from '../controllers/user.controller';

const router = Router(); // eslint-disable-line new-cap

router.route('')
    .get(UserController.getUsers);

router.route('/getUserByUsername/:username')
    .get(UserController.getUserByUsername);

router.route('/changeUserActiveById/:user_id')
    .put(UserController.changeUserActiveById);

router.route('')
    .post(UserController.addUser);

router.route('/:user_id')
    .put(UserController.updateUserById);

router.route('/:user_id')
    .delete(UserController.deleteUserById);

router.route('/addBulkUsers')
    .post(UserController.addBulkUsers);

router.route('/resetPasswordById')
    .post(UserController.resetPasswordById);

router.route('/getUsersByClassName/:class_name')
    .get(UserController.getUsersByClassName);

export default router;


