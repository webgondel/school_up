import { Router } from 'express';
const router = Router(); // eslint-disable-line new-cap
import * as uploadController from '../controllers/upload.controller';

router.route('/:image')
    /** GET /api/test - Test api */
    .get(uploadController.getImage);

export default router;
'upload/ss.png'
