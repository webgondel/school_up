import { Router } from 'express';
import * as ChatController from '../controllers/chat.controller';

const router = Router(); // eslint-disable-line new-cap

router.route('/getMessages')
    .post(ChatController.getMessages);

router.route('/sendMessage')
    .post(ChatController.sendMessage);

export default router;


