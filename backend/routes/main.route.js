import { Router } from 'express';
import * as MainController from '../controllers/main.controller';

const router = Router(); // eslint-disable-line new-cap

router.route('/addNickNameByUserId')
    .post(MainController.addNickNameByUserId);

router.route('/addSubject')
    .post(MainController.addSubject);

router.route('/getSubjects')
    .get(MainController.getSubjects);

router.route('/getSubTopicsBySubjectId/:subject_id')
    .get(MainController.getSubTopicsBySubjectId);

router.route('/addSubTopic')
    .post(MainController.addSubTopic);

router.route('/getPostsBySubTopicId/:sub_topic_id')
    .get(MainController.getPostsBySubTopicId);

router.route('/getPostByPostId/:post_id')
    .get(MainController.getPostByPostId);

router.route('/getRepliesByPostId/:post_id')
    .get(MainController.getRepliesByPostId);

router.route('/addPost')
    .post(MainController.addPost);

router.route('/addReply')
    .post(MainController.addReply);

router.route('/increaseViewsBySubjectId/:subject_id')
    .get(MainController.increaseViewsBySubjectId);

router.route('/increaseViewsBySubTopicId/:sub_topic_id')
    .get(MainController.increaseViewsBySubTopicId);

router.route('/increaseViewsByPostId/:post_id')
    .get(MainController.increaseViewsByPostId);

router.route('/getClouds')
    .get(MainController.getClouds);

router.route('/getCloudByCloudId/:cloud_id')
    .get(MainController.getCloudByCloudId);

router.route('/getPublicFolders')
    .get(MainController.getPublicFolders);

router.route('/getPrivateFoldersByUserId/:user_id')
    .get(MainController.getPrivateFoldersByUserId);

router.route('/addFolder')
    .post(MainController.addFolder);

router.route('/addCloud')
    .post(MainController.addCloud);

router.route('/addCalendarEvent')
    .post(MainController.addCalendarEvent);

router.route('/getCalendarEvents')
    .get(MainController.getCalendarEvents);

router.route('/deleteCalendarEventById/:event_id')
    .delete(MainController.deleteCalendarEventById);

router.route('/updateEvent/:event_id')
    .put(MainController.updateEvent);

router.route('/getPublicRootFiles')
    .get(MainController.getPublicRootFiles);

router.route('/getFilesByFolderId/:folder_id')
    .get(MainController.getFilesByFolderId);

router.route('/getPrivateRootFilesByUserId/:user_id')
    .get(MainController.getPrivateRootFilesByUserId);

router.route('/getPrivateFiles')
    .post(MainController.getPrivateFiles);

router.route('/getFolderByFolderId/:folder_id')
    .get(MainController.getFolderByFolderId);

router.route('/deleteFolderById/:folder_id')
    .delete(MainController.deleteFolderById);

router.route('/updateFolderById/:folder_id')
    .put(MainController.updateFolderById);

router.route('/deleteCloudById/:cloud_id')
    .delete(MainController.deleteCloudById);

router.route('/updateCloudById/:cloud_id')
    .put(MainController.updateCloudById);

router.route('/sendEmail')
    .post(MainController.sendEmail);

router.route('/addSecurityAnswers')
    .post(MainController.addSecurityAnswers);

router.route('/getSecurityAnswerByUserId/:user_id')
    .get(MainController.getSecurityAnswerByUserId);

router.route('/getChatRoomsByClass/:class_name')
    .get(MainController.getChatRoomsByClass);

router.route('/getChatRoomById/:room_id')
    .get(MainController.getChatRoomById);

router.route('/addChatRoom')
    .post(MainController.addChatRoom);

router.route('/deleteChatRoomById/:room_id')
    .delete(MainController.deleteChatRoomById);

router.route('/getRoomFoldersById/:room_id')
    .get(MainController.getRoomFoldersById);

router.route('/getRoomRootFilesById/:room_id')
    .get(MainController.getRoomRootFilesById);

export default router;

