import { Router } from 'express';

import testRoutes from './test.route';
import authRoutes from './auth.route';
import mainRoutes from './main.route';
import uploadRoutes from './upload.route';
import userRoutes from './user.route';
import chatRoutes from './chat.route';
import tokenDecodeMiddleware from './../middleware/tokenDecodeMiddleware';

const router = Router(); // eslint-disable-line new-cap

router.use('/test', testRoutes);
router.use('/auth', authRoutes);
router.use('/upload', uploadRoutes);

router.use(tokenDecodeMiddleware);

router.use('/main', mainRoutes);
router.use('/user', userRoutes);
router.use('/chat', chatRoutes);

export default router;
