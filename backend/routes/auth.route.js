import { Router } from 'express';

import * as AuthController from '../controllers/auth.controller';

const router = Router(); // eslint-disable-line new-cap

router.route('/login')
    .post(AuthController.login);

router.route('/signup')
    .post(AuthController.signup);

router.route('/resetRequest')
    .post(AuthController.resetRequest);

router.route('/resetPassword')
    .post(AuthController.resetPassword);

export default router;
