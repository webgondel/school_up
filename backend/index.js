import config from './config/config';
import app from './config/express';
const http = require('http').Server(app);
const io = require('socket.io')(http);
import {defineSocket} from "./sockets";

defineSocket(io);

http.listen(config.port, () => {
    console.info(`server started on port ${config.port} (${config.env})`); // eslint-disable-line no-console
});
