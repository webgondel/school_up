/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.8-MariaDB : Database - school_app
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`school_app` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `school_app`;

/*Table structure for table `cloud` */

DROP TABLE IF EXISTS `cloud`;

CREATE TABLE `cloud` (
  `file_name` varchar(100) NOT NULL,
  `origin_name` varchar(100) NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `cloud` */

insert  into `cloud`(`file_name`,`origin_name`,`user_id`) values 
('1585038141769.png','logo2.png',1),
('1585038141771.png','Multix-Icon.png',1),
('1585038141772.png','no-results.png',1),
('1585038218951.jpeg','card-audio.jpg',1);

/*Table structure for table `post_files` */

DROP TABLE IF EXISTS `post_files`;

CREATE TABLE `post_files` (
  `file_name` varchar(255) NOT NULL,
  `origin_name` varchar(255) NOT NULL,
  `post_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `post_files` */

insert  into `post_files`(`file_name`,`origin_name`,`post_id`) values 
('1584697267337.png','audio-img.png',2),
('1584697267338.jpeg','video-img2.jpg',2),
('1584699390494.pdf','Market place for kind of shopping mall.pdf',5),
('1584809000589.png','twitter.png',6);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `post_id` int(100) NOT NULL AUTO_INCREMENT,
  `sub_topic_id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_content` longtext NOT NULL,
  `views` int(100) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `posts` */

insert  into `posts`(`post_id`,`sub_topic_id`,`user_id`,`post_title`,`post_content`,`views`,`created_at`) values 
(2,2,1,'My first post','Hello, everyone!<div><br></div><div>We can learn more and more and share our experience in our platform.</div><div><br></div><div>Thank you very much</div><div><br></div><div>Dragon.</div>',18,'2020-03-24 14:51:44'),
(5,2,1,'What is stream?','I would like to know what live stream is.<div><br></div><div>Please let me have clean understand for this concept.</div><div><br></div><div>Thank you.</div><div><br></div><div>Best regards!</div>',16,'2020-03-24 14:51:38'),
(6,1,1,'Newtons first principle','This is very basic and important principle<div><br></div><div>Dragon</div><div><br></div><div>Thank you.</div>',1,'2020-03-22 00:43:42'),
(7,3,1,'How are you?','<b>Great</b><div><br></div><div>Thank you..</div><div><br></div><div>Dragon</div>',2,'2020-03-24 15:22:43');

/*Table structure for table `replies` */

DROP TABLE IF EXISTS `replies`;

CREATE TABLE `replies` (
  `reply_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `post_id` int(100) NOT NULL,
  `reply_content` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`reply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `replies` */

insert  into `replies`(`reply_id`,`user_id`,`post_id`,`reply_content`,`created_at`) values 
(2,1,5,'I know what stream is.<div><br></div><div>Please contact with me.</div><div><br></div><div>Thanks.</div>','2020-03-20 18:58:22'),
(3,1,5,'Hey,sir<div><br></div><div>There are 2 kinds of streams - live audio stream and live video stream</div><div><br></div><div>You can find a exact answer by contacting with me.</div><div><br></div><div>:D</div><div><br></div><div>Thank you.</div>','2020-03-20 18:59:42'),
(4,1,2,'Great post.<div><br></div><div>We will love our system.</div>','2020-03-20 19:48:12');

/*Table structure for table `reply_files` */

DROP TABLE IF EXISTS `reply_files`;

CREATE TABLE `reply_files` (
  `file_name` varchar(255) NOT NULL,
  `origin_name` varchar(255) NOT NULL,
  `reply_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `reply_files` */

insert  into `reply_files`(`file_name`,`origin_name`,`reply_id`) values 
('1584701902497.png','1584695851123.png',2),
('1584701902498.png','1584695851135.png',2),
('1584701982291.jpeg','1584695769863.jpeg',3),
('1584701982291.png','1584695769861.png',3),
('1584704892426.jpeg','card-audio.jpg',4);

/*Table structure for table `sub_topics` */

DROP TABLE IF EXISTS `sub_topics`;

CREATE TABLE `sub_topics` (
  `sub_topic_id` int(100) NOT NULL AUTO_INCREMENT,
  `subject_id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `sub_topic_title` varchar(255) NOT NULL,
  `views` int(100) NOT NULL DEFAULT 0,
  PRIMARY KEY (`sub_topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sub_topics` */

insert  into `sub_topics`(`sub_topic_id`,`subject_id`,`user_id`,`sub_topic_title`,`views`) values 
(1,3,1,'sss',2),
(2,2,1,'Gaussian Principle',32),
(3,2,1,'Integral',4),
(4,3,1,'bbb',3);

/*Table structure for table `subjects` */

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `subject_id` int(100) NOT NULL AUTO_INCREMENT,
  `subject_title` varchar(255) NOT NULL,
  `views` int(100) NOT NULL DEFAULT 0,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `subjects` */

insert  into `subjects`(`subject_id`,`subject_title`,`views`) values 
(2,'Mathematice',31),
(3,'Physics',7);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `class` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `role` enum('student','teacher','admin') DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Data for the table `users` */

insert  into `users`(`user_id`,`username`,`class`,`password`,`nickname`,`role`,`active`) values 
(1,'dragon','class-1','$2b$10$kR1Y5T/Zua1PNcICM7Zho.O6CBzNR4o2O3SEx6TneCviAPYGpFGEG','Dragon','admin',1),
(10,'test','class-2','$2b$10$BnSpA57AQTZwbPoL9ohI4OfiLj849JwcNrxZ26NgwfyPFh.CrBfFO','test','student',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
