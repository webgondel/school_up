import connection from '../config/database';
const chat_room_prefix = "x8dns9q_";

export const getMessages = (req, res) => {
    let {receiver_user_id, sender_user_id, is_group_chat, class_name} = req.body;
    console.log('class  ', class_name);
    if (is_group_chat === 0) {
        connection.query(`SELECT * FROM dragon_chat WHERE (receiver_user_id=${receiver_user_id} AND sender_user_id=${sender_user_id}) OR (receiver_user_id=${sender_user_id} AND sender_user_id=${receiver_user_id}) ORDER BY created_at`, function (error, results) {
            if (error) throw error;
            res.send(results);
        });
    } else {
        connection.query(`SELECT * FROM dragon_chat WHERE class='${class_name}' AND is_group_chat=1 ORDER BY created_at`, function (error, results) {
            if (error) throw error;
            res.send(results);
        });
    }
};

export const sendMessage = (req, res) => {
    let {receiver_user_id, message, sender_user_id, class_name, is_group_chat} = req.body;
    connection.query(`INSERT INTO dragon_chat (receiver_user_id, message, sender_user_id, class, is_group_chat) VALUES (${receiver_user_id}, '${message}', ${sender_user_id}, '${class_name}', ${is_group_chat})`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};
