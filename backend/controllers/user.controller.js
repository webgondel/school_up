import connection from '../config/database';
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../config/config";
const saltRounds = 10;

export const getUsers = (req, res) => {
    const role = req.user.role;
    console.log('---------->', role);

    if (role === 'admin') {
        connection.query(`SELECT * FROM users ORDER BY first_name`, function (error, results) {
            if (error) throw error;
            res.send(results);
        });
    }

    if (role === 'teacher') {
        connection.query(`SELECT * FROM users WHERE role='student' AND class='${req.user.class}' ORDER BY first_name`, function (error, results) {
            if (error) throw error;
            res.send(results);
        });
    }

    if (role === 'student') {
        res.status(401).send({
            message: 'Role is limited.'
        });
    }

};

export const changeUserActiveById = (req, res) => {
    const {user_id} = req.params;
    const {isChecked} = req.body;
    connection.query(`UPDATE users SET active=${isChecked} WHERE user_id=${user_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const addUser = (req, res) => {
    const {first_name, last_name, birthday, class_name, role, password} = req.body;
    bcrypt.hash(password, saltRounds, function(err, hash) {
        connection.query(`insert into users (first_name, last_name, birthday, class,  password, role) values('${first_name}', '${last_name}', '${birthday}', '${class_name}', '${hash}', '${role}')`, function (error, results) {
            if (error) {
                console.log('error >>>>>>>>>>', error);
                if(error.errno === 1062) {
                    res.status(400).send({
                        message: 'Username already exist!'
                    });
                }
            } else {
                res.send({success: true});
            }
        });
    });
};

export const deleteUserById = (req, res) => {
    const {user_id} = req.params;
    connection.query(`DELETE from users WHERE user_id=${user_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const updateUserById = (req, res) => {
    const {user_id} = req.params;
    const {first_name, last_name, birthday, nickname, class_name, role} = req.body;
    connection.query(`UPDATE users SET first_name='${first_name}', last_name='${last_name}', birthday='${birthday}', nickname='${nickname}', class='${class_name}', role='${role}' WHERE user_id=${user_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getUserByUsername = (req, res) => {
    const {username} = req.params;
    const first_name_first_letter = username.split('.')[0];
    const last_name = username.split('.')[1];
    connection.query(`SELECT * FROM users WHERE first_name LIKE '${first_name_first_letter}%' AND last_name='${last_name}' AND active=1`, function (error, results, fields) {
        if (error) throw error;
        const user = results[0];
        res.send({user});
    });
};

export const addBulkUsers = (req, res) => {
    const userLists = req.body;
    let k = 0;
    for (const user of userLists) {
        const password = user.birthday;
        const birthArray = user.birthday.split('.');
        const birthday = birthArray[2] + '-' + birthArray[1] + '-' + birthArray[0];
        bcrypt.hash(password, saltRounds, function(err, hash) {
            connection.query(`insert into users (first_name, last_name, class,  password, birthday) values('${user.first_name}', '${user.last_name}', '${user.class_name}', '${hash}', '${birthday}')`, function (error, results) {
                k++;
                if (error) {
                    console.log('error >>>>>>>>>>', error);
                    if(error.errno === 1062) {
                        // Username already exist!
                    }
                }
                if (k === userLists.length) {
                    res.send(true);
                }
            });
        });
    }

};

export const resetPasswordById = (req, res) => {
    const {user_id, password} = req.body;
    bcrypt.hash(password, saltRounds, function(err, hash) {
        connection.query(`UPDATE users SET password='${hash}' WHERE user_id=${user_id}`, function (error, results) {
            if (error) throw error;
            res.send(results);
        });
    });
};

export const getUsersByClassName = (req, res) => {
    const {class_name} = req.params;
    connection.query(`SELECT * FROM users WHERE class ='${class_name}' AND active=1`, function (error, results, fields) {
        if (error) throw error;
        res.send(results);
    });
};
