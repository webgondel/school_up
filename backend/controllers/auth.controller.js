import config from "../config/config";
import connection from '../config/database';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
const saltRounds = 10;
import uuid from 'uuid';
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

export const login = (req, res, next) => {
    const { username, password } = req.body;
    const first_name_first_letter = username.split('.')[0];
    const last_name = username.split('.')[1];
    connection.query(`SELECT * FROM users WHERE first_name LIKE '${first_name_first_letter}%' AND last_name='${last_name}' AND active=1`, function (error, results, fields) {
        if (error) throw error;
        const user = results[0];
        if(user) {
            bcrypt.compare(password, user.password, function(err, result) {
                if(result) {
                    // Passwords match
                    const token = jwt.sign({
                        user_id: user.user_id,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        class: user.class,
                        role: user.role
                    }, config.jwtSecret);

                    const userInfo = {
                        user_id: user.user_id,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        nickname: user.nickname,
                        class: user.class,
                        role: user.role
                    };

                    res.json({ token, user: userInfo });
                } else {
                    // Passwords don't match
                    res.status(400).send({
                        message: 'Password is wrong!'
                    });
                }
            });
        } else {
            res.status(400).send({
                message: `User doesn't exist!`
            });
        }
    });
};

export const signup = (req, res) => {
    const { first_name, last_name, class_name, password, role } = req.body;
    bcrypt.hash(password, saltRounds, function(err, hash) {
        connection.query(`insert into users (first_name, last_name, class, password, role) values('${first_name}', '${last_name}', '${class_name}', '${hash}', '${role}')`, function (error, results) {
            if (error) {
                console.log('error >>>>>>>>>>', error);
                if(error.errno === 1062) {
                    res.status(400).send({
                        message: 'Email already exist!'
                    });
                }
            } else {
                res.send({success: true});
            }
        });
    });
};

export const resetRequest = (req, res) => {
    const { email } = req.body;
    const uniqid = uuid.v1();
    connection.query(`update users set token='${uniqid}' where email='${email}'`, (error, results) => {
        if(results.affectedRows === 0) {
            res.status(400).send({
                message: "Email doesn't exist!"
            });
        } else {
            const msg = {
                to: email,
                from: process.env.SENDGRID_FROM_EMAIL,
                subject: 'REMAX Service',
                text: 'Please confirm your email',
                html: `
                <h1>Hello, how are you?</h1>
                <a href='${req.headers.origin}/resetPassword?token=${uniqid}'>Please click here to reset password.</a>
                <hr>
                <p>From Your REMAX Team</p>
              `,
            };
            sgMail.send(msg)
                .then(() => {
                    res.send({success: true});
                })
                .catch((error) => console.log('error', error));
        }
    });

};

export const resetPassword = (req, res) => {
    const {password, token} = req.body;
    bcrypt.hash(password, saltRounds, function(err, hash) {
        connection.query(`update users set password='${hash}' where token='${token}'`, function (error, results) {
            if (error) throw error;
            res.send({success: true})
        });
    });
};
