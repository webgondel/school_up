const path = require("path");

export const getImage = async (req, res) => {
    console.log('file route >>>>', req.params.image);
    res.sendFile(path.join(__dirname, `../uploads/${req.params.image}`));
};
