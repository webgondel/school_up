import connection from '../config/database';
import sgMail from "@sendgrid/mail";

export const addNickNameByUserId = (req, res) => {
    const {nickname, user_id} = req.body;
    connection.query(`UPDATE users set nickname='${nickname}' WHERE user_id=${user_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const addSubject = (req, res) => {
    const {subject_title} = req.body;
    connection.query(`INSERT INTO subjects (subject_title) VALUES ('${subject_title}')`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getSubjects = (req, res) => {
    connection.query(`SELECT subjects.subject_id, subjects.subject_title, subjects.views, sub_topics.sub_topic_id, COUNT(posts.post_id) counts FROM subjects LEFT JOIN sub_topics ON subjects.subject_id=sub_topics.subject_id LEFT JOIN posts ON sub_topics.sub_topic_id=posts.sub_topic_id GROUP BY subjects.subject_id  ORDER BY subject_title`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getSubTopicsBySubjectId = (req, res) => {
    const {subject_id} = req.params;
    connection.query(`SELECT sub_topics.sub_topic_id, sub_topics.subject_id, sub_topics.sub_topic_title, sub_topics.views, users.nickname, COUNT(posts.post_id) counts FROM sub_topics JOIN users ON sub_topics.user_id=users.user_id LEFT JOIN posts ON posts.sub_topic_id=sub_topics.sub_topic_id WHERE subject_id=${subject_id} GROUP BY sub_topics.sub_topic_id ORDER BY sub_topics.sub_topic_title `, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const addSubTopic = (req, res) => {
    const {sub_topic_title, user_id, subject_id} = req.body;
    connection.query(`INSERT INTO sub_topics (sub_topic_title, user_id, subject_id) VALUES ('${sub_topic_title}', ${user_id}, ${subject_id})`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getPostsBySubTopicId = (req, res) => {
    const {sub_topic_id} = req.params;
    connection.query(`SELECT posts.post_id, posts.sub_topic_id, posts.post_title, posts.post_content, posts.views, posts.created_at, users.nickname FROM posts JOIN users ON posts.user_id=users.user_id WHERE sub_topic_id=${sub_topic_id} ORDER BY posts.created_at`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getPostByPostId = (req, res) => {
    const {post_id} = req.params;
    console.log(post_id);
    connection.query(`SELECT posts.post_id, posts.sub_topic_id, posts.post_title, posts.post_content, posts.views, posts.created_at, users.nickname, post_files.origin_name, post_files.file_name FROM posts JOIN users ON posts.user_id=users.user_id LEFT JOIN post_files ON posts.post_id=post_files.post_id WHERE posts.post_id=${post_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const addPost = (req, res) => {
    const {post_title, post_content, user_id, sub_topic_id} = req.body;
    connection.query(`INSERT INTO posts (sub_topic_id, user_id, post_title, post_content) VALUES (${sub_topic_id}, ${user_id}, '${post_title}', '${post_content}')`, function (error, results) {
        if (error) throw error;
        const post_id = results['insertId'];
        req.files.forEach(file => {
            connection.query(`INSERT INTO post_files (file_name, origin_name, post_id) VALUES ('${file.filename}','${file.originalname}', ${post_id})`);
        });
        res.send(true);
    });
};

export const addReply = (req, res) => {
    const {reply_content, user_id, post_id} = req.body;
    connection.query(`INSERT INTO replies (user_id, post_id, reply_content) VALUES (${user_id}, ${post_id}, '${reply_content}')`, function (error, results) {
        if (error) throw error;
        const reply_id = results['insertId'];
        req.files.forEach(file => {
            connection.query(`INSERT INTO reply_files (file_name, origin_name, reply_id) VALUES ('${file.filename}','${file.originalname}', ${reply_id})`);
        });
        res.send(true);
    });
};

export const getRepliesByPostId = (req, res) => {
    const {post_id} = req.params;
    connection.query(`SELECT replies.reply_id, replies.reply_content, replies.created_at, users.nickname, reply_files.origin_name, reply_files.file_name FROM replies JOIN users ON replies.user_id=users.user_id LEFT JOIN reply_files ON replies.reply_id=reply_files.reply_id WHERE replies.post_id=${post_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const increaseViewsBySubjectId = (req, res) => {
    const {subject_id} = req.params;
    connection.query(`UPDATE subjects set views=views+1 WHERE subject_id=${subject_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const increaseViewsBySubTopicId = (req, res) => {
    const {sub_topic_id} = req.params;
    connection.query(`UPDATE sub_topics set views=views+1 WHERE sub_topic_id=${sub_topic_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const increaseViewsByPostId = (req, res) => {
    const {post_id} = req.params;
    connection.query(`UPDATE posts set views=views+1 WHERE post_id=${post_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getClouds = (req, res) => {
    connection.query(`SELECT cloud.file_name, cloud.origin_name, users.nickname FROM cloud JOIN users ON cloud.user_id=users.user_id ORDER BY cloud.origin_name`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getCloudByCloudId = (req, res) => {
    const {cloud_id} = req.params;
    connection.query(`SELECT * FROM cloud where cloud_id='${cloud_id}'`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const addCloud = (req, res) => {
    const {user_id, status, folder_id, room_id} = req.body;
    req.files.forEach(file => {
        const uniqid = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        connection.query(`INSERT INTO cloud (cloud_id, file_name, origin_name, user_id, folder_id, room_id, status) VALUES ('${uniqid}', '${file.filename}','${file.originalname}', ${user_id}, '${folder_id}', '${room_id}', '${status}')`);
    });
    res.send(true);
};

export const getPublicFolders = (req, res) => {
    connection.query(`SELECT folder.folder_id, folder.folder_name, folder.user_id, users.nickname FROM folder JOIN users ON folder.user_id=users.user_id WHERE status='public' ORDER BY folder.folder_name`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getPrivateFoldersByUserId = (req, res) => {
    const {user_id} =req.params;
    console.log('>>>>>>>>>>>>>>', user_id);
    connection.query(`SELECT folder.folder_id, folder.folder_name, folder.user_id, users.nickname FROM folder JOIN users ON folder.user_id=users.user_id WHERE status='private' and users.user_id=${user_id} ORDER BY folder.folder_name`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const addFolder = (req, res) => {
    const {user_id, folder_name, room_id, status} = req.body;
    const uniqid = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    connection.query(`INSERT INTO folder (folder_id, folder_name, user_id, status, room_id) VALUES ('${uniqid}', '${folder_name}', ${user_id}, '${status}', '${room_id}')`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const addCalendarEvent = (req, res) => {
    const data = req.body;
    connection.query(`INSERT INTO calendar (event_title, primary_color, secondary_color, start_at, end_at) VALUES ('${data.title}','${data.color.primary}', '${data.color.secondary}', '${data.start}', '${data.end}')`, function (error, results) {
        if (error) {console.log(error)}
        console.log(results['insertId']);
        res.send({ event_id: results['insertId']});
    });
};

export const getCalendarEvents = (req, res) => {
    connection.query(`SELECT * from calendar ORDER BY event_title`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const deleteCalendarEventById = (req, res) => {
    const {event_id} = req.params;
    connection.query(`DELETE FROM calendar WHERE event_id=${event_id}`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const updateEvent = (req, res) => {
    const {event_id} = req.params;
    const data = req.body;
    connection.query(`UPDATE calendar SET ${data.field}='${data.value}' WHERE event_id=${event_id}`);
    res.send(true);
};

export const getPublicRootFiles = (req, res) => {
    connection.query(`SELECT * from cloud WHERE folder_id=0 AND status='public' ORDER BY created_at`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getFilesByFolderId = (req, res) => {
    const {folder_id} = req.params;
    console.log('folder id >>>>>>>>>', folder_id);
    connection.query(`SELECT * from cloud WHERE folder_id='${folder_id}' ORDER BY created_at`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getPrivateRootFilesByUserId = (req, res) => {
    const {user_id} =req.params;
    connection.query(`SELECT * from cloud WHERE folder_id=0 AND status='private' AND user_id=${user_id} ORDER BY created_at`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getPrivateFiles = (req, res) => {
    const {folder_id, user_id} = req.body;
    connection.query(`SELECT * from cloud WHERE folder_id='${folder_id}' AND user_id=${user_id} ORDER BY created_at`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getFolderByFolderId = (req, res) => {
    const {folder_id} = req.params;
    connection.query(`SELECT * from folder WHERE folder_id='${folder_id}'`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const deleteFolderById = (req, res) => {
    const {folder_id} = req.params;
    connection.query(`DELETE FROM cloud WHERE folder_id='${folder_id}'`);
    connection.query(`DELETE FROM folder WHERE folder_id='${folder_id}'`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const updateFolderById = (req, res) => {
    const {folder_id} = req.params;
    const {folder_name} = req.body;
    connection.query(`UPDATE folder SET folder_name='${folder_name}' WHERE folder_id='${folder_id}'`);
    res.send(true);
};

export const deleteCloudById = (req, res) => {
    const {cloud_id} = req.params;
    connection.query(`DELETE FROM cloud WHERE cloud_id='${cloud_id}'`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const updateCloudById = (req, res) => {
    const {cloud_id} = req.params;
    const {origin_name} = req.body;
    console.log('>>>>>>>>>>>>', origin_name);
    connection.query(`UPDATE cloud SET origin_name='${origin_name}' WHERE cloud_id='${cloud_id}'`);
    res.send(true);
};

export const sendEmail = (req, res) => {
    const {email, content} = req.body;

    const template = `<p>${content}</p>`

    const option = {
        to: email,
        from: process.env.OWNER_EMAIL,
        subject: `SchoolUp Cloud Share`,
        text: 'share',
        html: template,
    };

    sgMail
        .send(option)
        .then(() => {
            console.log('email sending success...');
            res.send(true);
        })
        .catch((error) => {
            console.log('email sending error...', error);
            res.send(false);
        });
};

export const addSecurityAnswers = (req, res) => {
    const data = req.body;
    connection.query(`INSERT INTO security_answers (user_id, security_answer_1, security_answer_2, security_answer_3) VALUES (${data.user_id}, '${data.security_answer_1}', '${data.security_answer_2}', '${data.security_answer_3}')`, function (error, results) {
        if (error) {console.log(error)}
        res.send(results);
    });
};

export const getSecurityAnswerByUserId = (req, res) => {
    const {user_id} = req.params;
    connection.query(`SELECT * from security_answers WHERE user_id=${user_id}`, function (error, results) {
        if (error) {console.log(error)}
        res.send(results[0]);
    });
};

export const getChatRoomsByClass = (req, res) => {
    const {class_name} = req.params;
    connection.query(`SELECT * from chat_room WHERE class='${class_name}'`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getChatRoomById = (req, res) => {
    const {room_id} = req.params;
    connection.query(`SELECT * from chat_room WHERE room_id='${room_id}'`, function (error, results) {
        if (error) throw error;
        res.send(results[0]);
    });
};

export const addChatRoom = (req, res) => {
    const {class_name, room_name} = req.body;
    const uniqid = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    connection.query(`INSERT INTO chat_room (room_id, room_name, class) VALUES ('${uniqid}', '${room_name}', '${class_name}')`, function (error, results) {
        if (error) {console.log(error)}
        res.send(results);
    });
};

export const deleteChatRoomById = (req, res) => {
    const {room_id} = req.params;
    connection.query(`DELETE FROM chat_room WHERE room_id='${room_id}'`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getRoomFoldersById = (req, res) => {
    const {room_id} = req.params;
    console.log('---------->', room_id);
    connection.query(`SELECT * from folder WHERE room_id='${room_id}'`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};

export const getRoomRootFilesById = (req, res) => {
    const {room_id} = req.params;
    connection.query(`SELECT * from cloud WHERE room_id='${room_id}' AND folder_id='0'`, function (error, results) {
        if (error) throw error;
        res.send(results);
    });
};
