export const environment = {
  production: true,
  // baseUrl: 'http://88.198.203.57/',
  // apiUrl: 'http://88.198.203.57/api/'
  baseUrl: 'https://app.school-up.de/',
  apiUrl: 'https://app.school-up.de/api/',
  openvidu_url: 'https://video.school-up.de',
  openvidu_secret: 'MY_SECRET'
};
