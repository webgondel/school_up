import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UrlJSON} from "../utils/UrlJSON";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  getUsers() {
    return this.http.get(`${UrlJSON.userUrl}`);
  }

  getUserByUsername(username) {
    return this.http.get(`${UrlJSON.userUrl}/getUserByUsername/${username}`);
  }

  getUserById(id) {
    return this.http.get(`${UrlJSON.userUrl}/${id}`);
  }

  deleteUserById(id) {
    return this.http.delete(`${UrlJSON.userUrl}/${id}`);
  }

  updateUserById(id, data) {
    return this.http.put(`${UrlJSON.userUrl}/${id}`, data);
  }

  changeUserActiveById(id, isChecked) {
    return this.http.put(`${UrlJSON.userUrl}/changeUserActiveById/${id}`, {isChecked});
  }

  addUser(data) {
    return this.http.post(`${UrlJSON.userUrl}`, data);
  }

  resetPasswordById(data) {
    return this.http.post(`${UrlJSON.userUrl}/resetPasswordById`, data);
  }

  getUsersByClassName(class_name) {
    return this.http.get(`${UrlJSON.userUrl}/getUsersByClassName/${class_name}`);
  }

}
