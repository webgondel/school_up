import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {UrlJSON} from '../utils/UrlJSON';
import {Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthService {
  public currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('schoolCurrentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  updateCurrentUser(data) {
    localStorage.setItem('schoolCurrentUser', JSON.stringify(data));
    this.currentUserSubject.next(data);
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${UrlJSON.loginUrl}`, {username, password})
      .pipe(map(response => {
        // login successful if there's a jwt token in the response
        if (response) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('schoolCurrentUser', JSON.stringify(response));
          this.currentUserSubject.next(response);
        }
        return response;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('schoolCurrentUser');
    this.currentUserSubject.next(null);
  }
}
