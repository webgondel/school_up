import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {UrlJSON} from '../utils/UrlJSON';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  addNickNameByUserId(nickname, id) {
    return this.http.post(`${UrlJSON.mainUrl}/addNickNameByUserId`, {nickname, user_id: id});
  }

  addSubject(subject_title) {
    return this.http.post(`${UrlJSON.mainUrl}/addSubject`, {subject_title});
  }

  getSubjects() {
    return this.http.get(`${UrlJSON.mainUrl}/getSubjects`);
  }

  getSubTopicsBySubjectId(subject_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getSubTopicsBySubjectId/${subject_id}`);
  }

  addSubTopic(sub_topic_title, user_id, subject_id) {
    return this.http.post(`${UrlJSON.mainUrl}/addSubTopic`, {sub_topic_title, user_id, subject_id});
  }

  getPostsBySubTopicId(sub_topic_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getPostsBySubTopicId/${sub_topic_id}`);
  }

  getPostByPostId(post_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getPostByPostId/${post_id}`);
  }

  getRepliesByPostId(post_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getRepliesByPostId/${post_id}`);
  }

  addPost(data) {
    return this.http.post(`${UrlJSON.mainUrl}/addPost`, data);
  }

  addReply(data) {
    return this.http.post(`${UrlJSON.mainUrl}/addReply`, data);
  }

  increaseViewsBySubjectId(subject_id) {
    return this.http.get(`${UrlJSON.mainUrl}/increaseViewsBySubjectId/${subject_id}`);
  }

  increaseViewsBySubTopicId(sub_topic_id) {
    return this.http.get(`${UrlJSON.mainUrl}/increaseViewsBySubTopicId/${sub_topic_id}`);
  }

  increaseViewsByPostId(post_id) {
    return this.http.get(`${UrlJSON.mainUrl}/increaseViewsByPostId/${post_id}`);
  }

  getClouds() {
    return this.http.get(`${UrlJSON.mainUrl}/getClouds`);
  }

  getCloudByCloudId(cloud_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getCloudByCloudId/${cloud_id}`);
  }

  addCloud(data) {
    return this.http.post(`${UrlJSON.mainUrl}/addCloud`, data);
  }

  getPublicFolders() {
    return this.http.get(`${UrlJSON.mainUrl}/getPublicFolders`);
  }

  getPrivateFoldersByUserId(user_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getPrivateFoldersByUserId/${user_id}`);
  }

  getFolderByFolderId(folder_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getFolderByFolderId/${folder_id}`);
  }

  getPublicRootFiles() {
    return this.http.get(`${UrlJSON.mainUrl}/getPublicRootFiles`);
  }

  getFilesByFolderId(folder_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getFilesByFolderId/${folder_id}`);
  }

  getPrivateRootFilesByUserId(user_id) {
    return this.http.get(`${UrlJSON.mainUrl}/getPrivateRootFilesByUserId/${user_id}`);
  }

  getPrivateFiles(data) {
    return this.http.post(`${UrlJSON.mainUrl}/getPrivateFiles`, data);
  }

  addFolder(data) {
    return this.http.post(`${UrlJSON.mainUrl}/addFolder`, data);
  }

  addCalendarEvent(data) {
    return this.http.post(`${UrlJSON.mainUrl}/addCalendarEvent`, data);
  }

  getCalendarEvents() {
    return this.http.get(`${UrlJSON.mainUrl}/getCalendarEvents`);
  }

  deleteCalendarEventById(id) {
    return this.http.delete(`${UrlJSON.mainUrl}/deleteCalendarEventById/${id}`);
  }

  updateEvent(id, data) {
    return this.http.put(`${UrlJSON.mainUrl}/updateEvent/${id}`, data);
  }

  downloadCloud(file_name) {
    return this.http.get(`${UrlJSON.cloudUrl}/${file_name}`, {
      observe: 'response',
      responseType: 'blob'
    });
  }

  updateFolderById(id, data) {
    return this.http.put(`${UrlJSON.mainUrl}/updateFolderById/${id}`, data);
  }

  deleteFolderById(id) {
    return this.http.delete(`${UrlJSON.mainUrl}/deleteFolderById/${id}`);
  }

  updateCloudById(id, data) {
    return this.http.put(`${UrlJSON.mainUrl}/updateCloudById/${id}`, data);
  }

  deleteCloudById(id) {
    return this.http.delete(`${UrlJSON.mainUrl}/deleteCloudById/${id}`);
  }

  sendEmail(data) {
    return this.http.post(`${UrlJSON.mainUrl}/sendEmail`, data);
  }

  addBulkUsers(data) {
    return this.http.post(`${UrlJSON.userUrl}/addBulkUsers`, data);
  }

  getSecurityQuestions() {
    return this.http.get(`${UrlJSON.mainUrl}/getSecurityQuestions`);
  }

  addSecurityAnswers(data) {
    return this.http.post(`${UrlJSON.mainUrl}/addSecurityAnswers`, data);
  }

  getSecurityAnswerByUserId(id) {
    return this.http.get(`${UrlJSON.mainUrl}/getSecurityAnswerByUserId/${id}`);
  }

  getChatRoomsByClass(class_name) {
    return this.http.get(`${UrlJSON.mainUrl}/getChatRoomsByClass/${class_name}`);
  }

  getChatRoomById(id) {
    return this.http.get(`${UrlJSON.mainUrl}/getChatRoomById/${id}`);
  }

  addChatRoom(data) {
    return this.http.post(`${UrlJSON.mainUrl}/addChatRoom`, data);
  }

  deleteChatRoomById(id) {
    return this.http.delete(`${UrlJSON.mainUrl}/deleteChatRoomById/${id}`);
  }

  getRoomFoldersById(id) {
    return this.http.get(`${UrlJSON.mainUrl}/getRoomFoldersById/${id}`);
  }

  getRoomRootFilesById(id) {
    return this.http.get(`${UrlJSON.mainUrl}/getRoomRootFilesById/${id}`);
  }

}
