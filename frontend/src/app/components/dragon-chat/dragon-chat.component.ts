import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDrawer} from "@angular/material/sidenav";
import {ChatService} from "./services/chat.service";

@Component({
  selector: 'app-dragon-chat',
  templateUrl: './dragon-chat.component.html',
  styleUrls: ['./dragon-chat.component.scss']
})
export class DragonChatComponent implements OnInit {

  @ViewChild('drawer', {static: false}) drawer: MatDrawer;
  opened = true;
  mode = 'side';
  selectedRoom;

  constructor(
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.detectDeviceWidth();
    this.chatService.selectedRoom.subscribe((res:any) => {
      this.selectedRoom = res.roomInfo;
      console.log('res------>', res);
    });
  }

  detectDeviceWidth() {
    const deviceWidth = window.innerWidth;
    if (deviceWidth <= 768) {
      this.opened = false;
      this.mode = "over";
    } else {
      this.opened = true;
      this.mode = "side";
    }
  }

  onOpenStatus() {
    console.log('working.............');
    this.drawer.toggle();
  }

}
