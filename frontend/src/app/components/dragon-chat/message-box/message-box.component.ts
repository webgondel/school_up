import { Component, OnInit  } from '@angular/core';
import {ChatService} from "../services/chat.service";
import {AuthService} from "../../../services/auth.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss']
})
export class MessageBoxComponent implements OnInit  {

  selectedRoom;
  messageLists = [];
  currentUser;
  typingMessage = '';
  _sub: Subscription;

  constructor(
    private chatService: ChatService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.defineDomEvent();
    this.currentUser = this.authService.currentUserValue.user;
    this.chatService.selectedRoom.subscribe((res:any) => {
      this.selectedRoom = res.roomInfo;
      this.getMessages();
    });
    this.chatService.newReceiverMessage.subscribe(res => {
      console.log('receiving message ....');
      this.receiveNewMessage(res);
    });
  }

  defineDomEvent() {
    const taFrame = document.querySelector('#ta-frame');
    const ta =  document.querySelector('textarea');
    ta.addEventListener('keydown', autosize);

    function autosize(e) {
      if (e.keyCode === 13 && !e.shiftKey) {
        setTimeout(function() {
          ta.style.cssText = 'height:0px';
          const height = 40;
          // @ts-ignore
          taFrame.style.cssText = 'height:' + height + 'px';
          ta.style.cssText = 'height:' + height + 'px';
        },0);
      } else {
        setTimeout(function() {
          ta.style.cssText = 'height:0px';
          const height = Math.min(20 * 5, ta.scrollHeight);
          // @ts-ignore
          taFrame.style.cssText = 'height:' + height + 'px';
          ta.style.cssText = 'height:' + height + 'px';
          console.log('hegiht,', height);
        },0);
      }
    }
  }

  getMessages() {
    this.chatService.getMessages({
      receiver_user_id: this.selectedRoom.user_id,
      sender_user_id: this.currentUser.user_id,
      is_group_chat: this.selectedRoom.is_group_chat,
      class_name: this.selectedRoom.class_name
    }).subscribe((res: any) => {
      this.messageLists = res;
      console.log('messages', res);
      this.changeMessageBoxScrollBar();
    });
  }

  handleTypingMessage(e) {
    if (!this.typingMessage) {
      return;
    }
    if (e.keyCode === 13 && !e.shiftKey) {
      e.preventDefault();
      const now = new Date();
      const messageData = {
        message: this.typingMessage,
        receiver_user_id: this.selectedRoom.user_id,
        sender_user_id: this.currentUser.user_id,
        is_group_chat: this.selectedRoom.is_group_chat,
        class_name: this.selectedRoom.class_name,
        created_at: now.toISOString()
      };
      this.typingMessage = '';
      this.chatService.sendMessage(messageData).subscribe();
      this.chatService.emitMessageToReceiver(messageData);
      this.updateMessageLists(messageData);
    }
  }

  updateMessageLists(messageData) {
    this.messageLists.push(messageData);
    this.changeMessageBoxScrollBar();
  }

  changeMessageBoxScrollBar() {
    setTimeout(() => {
      const objDiv = document.getElementById("message-box-content");
      objDiv.scrollTop = objDiv.scrollHeight
    }, 0);
  }

  receiveNewMessage(messageData) {
    console.log('messageData', messageData);
    console.log('selectedRoom', this.selectedRoom);
    if ((this.selectedRoom.is_group_chat === 1 && messageData.is_group_chat ===1) || messageData.sender_user_id === this.selectedRoom.user_id) {
      this.updateMessageLists(messageData);
    }
  }

}
