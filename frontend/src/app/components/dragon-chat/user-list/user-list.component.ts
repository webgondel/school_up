import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {UserService} from "../../../services/user.service";
import {ChatService} from "../services/chat.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  @Input() opened: boolean;
  @Output() openStatus = new EventEmitter();
  userLists = [];

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.getUsersByClassName();
  }

  handleOpenStatus() {
    this.openStatus.emit();
  }

  getUsersByClassName() {
    this.userService.getUsersByClassName(this.authService.currentUserValue.user.class).subscribe((res: any) => {
      this.userLists = res.filter(item => item.user_id !== this.authService.currentUserValue.user.user_id);
    })
  }

  handleClickUserList(user) {
    this.chatService.changeRoom({roomInfo: {
        user_id: user.user_id,
        chatRoomTitle: user.first_name + user.last_name,
        is_group_chat: 0,
        class_name: this.authService.currentUserValue.user.class
      }
    });
  }

  handleClickClassRoom() {
    this.chatService.changeRoom({roomInfo: {
        user_id: 0,
        chatRoomTitle: 'Class Room',
        is_group_chat: 1,
        class_name: this.authService.currentUserValue.user.class
      }
    });
  }


}
