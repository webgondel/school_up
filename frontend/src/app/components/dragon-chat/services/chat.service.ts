import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {UrlJSON} from "../../../utils/UrlJSON";
import {Socket} from 'ngx-socket-io';
import {AuthService} from "../../../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public selectedRoom = new BehaviorSubject({});
  newReceiverMessage = this.socket.fromEvent('newMessage');
  currentUser;

  changeRoom(data) {
    this.selectedRoom.next(data);
  }

  constructor(
    private http: HttpClient,
    private socket: Socket,
    private authService: AuthService
  ) {
    console.log('chat service .........');
    this.currentUser = this.authService.currentUserValue.user;
  }

  getMessages(data) {
    return this.http.post(`${UrlJSON.chatUrl}/getMessages`, data);
  }

  sendMessage(data) {
    return this.http.post(`${UrlJSON.chatUrl}/sendMessage`, data);
  }

  // socket service

  setChatRoom() {
    this.socket.emit("setChatRoom", this.currentUser.user_id);
  }

  emitMessageToReceiver(messageData) {
    console.log('emitting...');
    this.socket.emit("newMessage", messageData);
  }

}
