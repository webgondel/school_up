import {Component, ChangeDetectionStrategy, ViewChild, TemplateRef, Injectable} from '@angular/core';
import {startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView} from 'angular-calendar';
import Flatpickr from 'flatpickr';
import {German} from "flatpickr/dist/l10n/de";
import {ApiService} from "../../services/api.service";




Flatpickr.localize(German);

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent {
  @ViewChild('modalContent', { static: false }) modalContent: TemplateRef<any>;

  locale: string = 'he';

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  // events: CalendarEvent[] = [
  //   {
  //     start: subDays(startOfDay(new Date()), 1),
  //     end: addDays(new Date(), 3),
  //     title: 'Eine 3-tägige Veranstaltung',
  //     color: colors.red,
  //   }
  // ];
  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = true;

  // constructor(private modal: NgbModal) {}
  constructor(
    private apiService: ApiService
  ) {
    this.getCalendarEvents();
  }

  getCalendarEvents() {
    this.apiService.getCalendarEvents().subscribe((res:any) => {
      res.forEach(item => {
        this.events.push({
            id: item.event_id,
            start: (new Date(item.start_at)),
            end: (new Date(item.end_at)),
            title: item.event_title,
            color: {primary: item.primary_color, secondary: item.secondary_color},
        });
      });
      this.refresh.next();
    })
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    console.log('clicked event...');
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
                      event,
                      newStart,
                      newEnd
                    }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
  }

  addEvent(): void {
    const newEvent = {
      id: 0,
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
    };
    console.log('>>>>>>>>>');
    this.apiService.addCalendarEvent(newEvent).subscribe((res: any) => {
      console.log(res.event_id);
      newEvent.id = res.event_id;
      console.log('newEvent >>>>>>>>', newEvent);
      this.events = [
        ...this.events,
        newEvent
      ];
    });
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
    this.apiService.deleteCalendarEventById(eventToDelete.id).subscribe();
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  updateEvent(id, field, value) {
    this.refresh.next();
    console.log(id, field, value);
    this.apiService.updateEvent(id, {field, value}).subscribe();
  }
}
