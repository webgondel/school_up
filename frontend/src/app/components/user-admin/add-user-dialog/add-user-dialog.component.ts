import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {AuthService} from "../../../services/auth.service";
import {UserService} from "../../../services/user.service";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss']
})
export class AddUserDialogComponent implements OnInit {

  currentUser: any;
  role_lists = [
    {name: 'Schüler', value: 'student'},
    {name: 'Lehrer', value: 'teacher'},
    {name: 'Administrator', value: 'admin'},
  ];
  addForm: FormGroup;
  submitted = false;
  formData: any;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AddUserDialogComponent>) {}

  get f() { return this.addForm.controls; }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addUser(){
    this.submitted = true;
    if(this.addForm.invalid) {
      return
    }
    if(this.f.password.value !== this.f.cpassword.value) {
      this.f.cpassword.setErrors({'nomatch': true});
      return
    }
    this.formData = {
      first_name: this.f.first_name.value,
      last_name: this.f.last_name.value,
      birthday: this.f.birthday.value,
      class_name: this.f.class.value,
      role: this.f.roles.value || 'student',
      password: this.f.password.value,
    };
    this.userService.addUser(this.formData).subscribe(response => {
      if(response) {
        this.dialogRef.close(this.formData);
      }
    });
  }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      password: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      birthday: ['', Validators.required],
      class: ['', Validators.required],
      roles: [''],
      cpassword: ['', Validators.required],
    });
    this.currentUser = this.authService.currentUserValue;
  }

}
