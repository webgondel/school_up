import {Component, OnInit, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../services/auth.service";
import {UserService} from "../../../services/user.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent implements OnInit {

  editForm: FormGroup;
  submitted = false;
  role_lists = [
    {name: 'Schüler', value: 'student'},
    {name: 'Lehrer', value: 'teacher'},
    {name: 'Administrator', value: 'admin'},
  ];
  formData: any;

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditUserDialogComponent>) {}

  get f() { return this.editForm.controls; }

  onNoClick(): void {
    this.dialogRef.close();
  }

  editUser(){
    this.submitted = true;
    if(this.editForm.invalid) {
      return
    }
    this.formData = {
      first_name: this.f.first_name.value,
      last_name: this.f.last_name.value,
      birthday: this.f.birthday.value,
      nickname: this.f.nickname.value,
      class_name: this.f.class.value,
      role: this.f.roles.value || 'student',
    };
    this.userService.updateUserById(this.data.user_id, this.formData).subscribe(response => {
      if(response) {
        this.dialogRef.close(this.formData);
      }
    });
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      first_name: [this.data.first_name, Validators.required],
      last_name: [this.data.last_name, Validators.required],
      birthday: [this.data.birthday, Validators.required],
      nickname: [this.data.nickname],
      class: [this.data.class, Validators.required],
      roles: [this.data.role],
    });
  }
}
