import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {SnackBarNotification} from "../../utils/snack-bar-notification";
import {AuthService} from "../../services/auth.service";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmDialogBoxComponent} from "../common/confirm-dialog-box/confirm-dialog-box.component";
import {ApiService} from "../../services/api.service";
import {UserService} from "../../services/user.service";
import {EditUserDialogComponent} from "./edit-user-dialog/edit-user-dialog.component";
import {AddUserDialogComponent} from "./add-user-dialog/add-user-dialog.component";
import {AddMultiUsersDialogComponent} from "./add-multi-users-dialog/add-multi-users-dialog.component";

@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss']
})
export class UserAdminComponent implements OnInit {

  currentUser: any;
  // table configuration
  displayedColumns: string[] = ['first_name', 'last_name', 'nickname', 'class', 'role', 'birthday', 'active', 'action'];
  dataSource: any = new MatTableDataSource<any>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  roleName = {
    student: 'Schüler',
    teacher: 'Lehrer',
    admin: 'Administrator'
  };

  constructor(
    private snackBarNotification: SnackBarNotification,
    private authService: AuthService,
    public dialog: MatDialog,
    private apiService: ApiService,
    private userService: UserService
  ) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  confirmDialog(id): void {
    const dialogRef = this.dialog.open(ConfirmDialogBoxComponent, {
      width: '500px',
      data: {alarmMessage: 'Wollen Wie diesen Benutzer wirklich entfernen?'}
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if(confirm) {
        this.deleteChannelById(id);
      }
    });
  }

  deleteChannelById(id) {
    this.dataSource.data = this.dataSource.data.filter(item => item.user_id!==id);
    this.userService.deleteUserById(id).subscribe(success => {
      if(success) {
        this.snackBarNotification.message(this.snackBarNotification.content.deleted);
      }
    });
  }

  changeSlideToggle(id, isChecked) {
    if (isChecked) {
      isChecked = 1
    } else {
      isChecked = 0;
    }
    console.log(id);
    this.userService.changeUserActiveById(id, isChecked).subscribe(success => {
      if(success) {
        this.snackBarNotification.message(this.snackBarNotification.content.updated);
      }
    });
  }

  addUser () {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      width: '65%',

    });
    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined) {
        this.userService.getUsers().subscribe((data) => {
          this.dataSource.data = data;
        });
        this.snackBarNotification.message(this.snackBarNotification.content.created);
      }
    });
  }

  editUser(id) {
    const thisRow = this.dataSource.data.find(item => item.user_id===id);
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      width: '65%',
      data: thisRow
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined) {
        // const index = this.dataSource.data.findIndex((e) => e.user_id === id);
        // for(let [key, value] of Object.entries(result)) {
        //   if (key === 'class_name') {let key = 'class'};
        //   this.dataSource.data[index][key] = value;
        // }
        this.userService.getUsers().subscribe((data) => {
          this.dataSource.data = data;
        });
        console.log('----------->', this.dataSource.data);
        this.snackBarNotification.message(this.snackBarNotification.content.updated);
      }
    });
  }

  addUsers() {
    const dialogRef = this.dialog.open(AddMultiUsersDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(confirm => {
      this.getUsers();
    });
  }

  getUsers() {
    this.userService.getUsers().subscribe((data) => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.data = data;
    });
  }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
    this.getUsers();
  }
}
