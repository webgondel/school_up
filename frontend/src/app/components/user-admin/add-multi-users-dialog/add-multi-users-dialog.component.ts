import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import * as XLSX from 'xlsx';
import {ApiService} from "../../../services/api.service";

@Component({
  selector: 'app-add-multi-users-dialog',
  templateUrl: './add-multi-users-dialog.component.html',
  styleUrls: ['./add-multi-users-dialog.component.scss']
})
export class AddMultiUsersDialogComponent implements OnInit {

  isEmptyFile = true;
  userLists;
  loading = false;
  isSuccess = false;

  constructor(
    public dialogRef: MatDialogRef<AddMultiUsersDialogComponent>,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onFileChange(ev) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      this.isEmptyFile = false;
      this.userLists = jsonData.Sheet1;
    };
    reader.readAsBinaryString(file);
  }

  addBulkUsers() {
    this.loading = true;
    this.apiService.addBulkUsers(this.userLists).subscribe(res => {
      this.isSuccess = true;
      this.loading = false;
    });
  }

}
