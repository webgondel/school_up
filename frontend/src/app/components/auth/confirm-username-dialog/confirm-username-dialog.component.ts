import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {ApiService} from "../../../services/api.service";
import {AuthService} from "../../../services/auth.service";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-confirm-username-dialog',
  templateUrl: './confirm-username-dialog.component.html',
  styleUrls: ['./confirm-username-dialog.component.scss']
})
export class ConfirmUsernameDialogComponent implements OnInit {

  error = '';

  myFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(
    public dialogRef: MatDialogRef<ConfirmUsernameDialogComponent>,
    private apiService: ApiService,
    private userService: UserService,
    private authService: AuthService
  ) { }

  ngOnInit() {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addValue() {
    if (this.myFormControl.invalid) {
      return;
    }
    const value = this.myFormControl.value;
    this.userService.getUserByUsername(value).subscribe((res:any) => {
      if (!res.user) {
        this.error = "User doesn't exist";
      } else if (!res.user.nickname) {
        this.error = "You can't reset password because you didn't set security questions yet.";
      } else {
        this.dialogRef.close(res.user.user_id);
      }
    })
  }

}
