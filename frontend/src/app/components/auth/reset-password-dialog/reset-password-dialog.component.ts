import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ApiService} from "../../../services/api.service";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-reset-password-dialog',
  templateUrl: './reset-password-dialog.component.html',
  styleUrls: ['./reset-password-dialog.component.scss']
})
export class ResetPasswordDialogComponent implements OnInit {

  form: FormGroup;
  submitted;
  error = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private dialogRef: MatDialogRef<ResetPasswordDialogComponent>
  ) { }

  get f() { return this.form.controls; }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      password: ['', Validators.required],
      cpassword: ['', Validators.required],
    });
  }

  confirm() {
    this.submitted = true;
    if(this.form.invalid) {
      return
    }
    if (this.f.password.value !== this.f.cpassword.value) {
      this.error = "Password doesn't match.";
      return;
    }
    this.userService.resetPasswordById({user_id: this.data.user_id, password: this.f.password.value}).subscribe(res => {
      this.dialogRef.close(true);
    });
  }

}
