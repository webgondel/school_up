import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

import { first } from 'rxjs/operators';
import {AddSubjectDialogComponent} from "../../forum/subject/add-subject-dialog/add-subject-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmUsernameDialogComponent} from "../confirm-username-dialog/confirm-username-dialog.component";
import {ConfirmSecurityDialogComponent} from "../confirm-security-dialog/confirm-security-dialog.component";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ResetPasswordDialogComponent} from "../reset-password-dialog/reset-password-dialog.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  formData: any;
  error = '';
  currentUser;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog,
    private snackBarNotification: SnackBarNotification
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.currentUser = this.authService.currentUserValue;
    console.log('??????????????', this.currentUser);
    if (this.currentUser) {
      this.router.navigate(['/home']);
    }
  }

  get f() { return this.form.controls; }

  submitForm() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.login();
  }

  login() {
    this.authService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('login success >>>>>>>>>>>>', data);
          this.router.navigate(['/home']);
        },
        error => {
          this.error = error.error.message;
        });
  }

  openConfirmUsernameDialog() {
    const dialogRef = this.dialog.open(ConfirmUsernameDialogComponent, {
      width: '500px',
    });
    dialogRef.afterClosed().subscribe(user_id => {
      if (user_id) {
        console.log('existing user >>>>>>>>', user_id);
        this.openConfirmSecurityDialog(user_id)
      }
    });
  }

  openConfirmSecurityDialog(user_id) {
    const dialogRef = this.dialog.open(ConfirmSecurityDialogComponent, {
      width: '700px',
      data: {user_id}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.openResetPasswordDialog(user_id)
      }
    });
  }

  openResetPasswordDialog(user_id) {
    const dialogRef = this.dialog.open(ResetPasswordDialogComponent, {
      width: '700px',
      data: {user_id}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.updated);
      }
    });
  }

}
