import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ApiService} from "../../../services/api.service";

@Component({
  selector: 'app-confirm-security-dialog',
  templateUrl: './confirm-security-dialog.component.html',
  styleUrls: ['./confirm-security-dialog.component.scss']
})
export class ConfirmSecurityDialogComponent implements OnInit {

  securityAnswers = [];
  answer = '';
  error= '';
  nthAnswer = 0;
  securityQuestions = [
    'Was ist / war der Name von Deinem Lieblingsbuch?',
    'Was ist / war der Name des ersten Haustiers?',
    'Was ist / war der Name von Deinem Lieblingsfilm?'
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ConfirmSecurityDialogComponent>,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.apiService.getSecurityAnswerByUserId(this.data.user_id).subscribe((res:any) => {
      this.securityAnswers[0] = res.security_answer_1;
      this.securityAnswers[1] = res.security_answer_2;
      this.securityAnswers[2] = res.security_answer_3;
      console.log('------->', this.securityAnswers);
    });
  }

  confirm() {
    if (this.answer === '') {
      return;
    }
    this.dialogRef.close(true);
    console.log('?????', this.answer, this.securityAnswers[this.nthAnswer]);
    if (this.answer === this.securityAnswers[this.nthAnswer]) {
      console.log('true');
      this.dialogRef.close(true);
    } else {
      console.log('false');
      if (this.nthAnswer === 2) {
        this.error = "All of the security answers are wrong. You can't reset password."
      } else {
        this.answer = '';
        this.nthAnswer++;
      }
    }
  }



}
