import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../../../../services/api.service";
import {environment} from "../../../../../environments/environment";
import {parseDate} from '../../../../helper/functions';
import {Location} from "@angular/common";

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  post_id;
  replies = [];
  uploadUrl = environment.apiUrl + 'upload/';
  post = {
    post_title: '',
    post_content: '',
    nickname: '',
    created_at: null,
  };
  post_file_lists = [];
  reply_file_lists = [];
  parseDate = parseDate;
  sub_topic_title;
  subject_title;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.post_id = this.route.snapshot.queryParamMap.get('post_id');
    this.sub_topic_title = this.route.snapshot.queryParamMap.get('sub_topic_title');
    this.subject_title = this.route.snapshot.queryParamMap.get('subject_title');
    this.getPostByPostId();
    this.getRepliesByPostId();
  }

  getPostByPostId() {
    this.apiService.getPostByPostId(this.post_id).subscribe((res:any) => {
      this.post = res[0];
      res.forEach(item => {
        if (item.file_name !== null) {
          this.post_file_lists.push({
            file_name: item.file_name,
            origin_name: item.origin_name
          })
        }
      });
    });
  }

  getRepliesByPostId() {
    this.apiService.getRepliesByPostId(this.post_id).subscribe((res:any) => {
      res.forEach(item => {
        if(this.replies.findIndex(reply => reply.reply_id === item.reply_id) === -1) {
          this.replies.push(item);
          this.reply_file_lists[item.reply_id] = [];
        }
        this.reply_file_lists[item.reply_id].push({origin_name: item.origin_name, file_name: item.file_name});
      });
      console.log(this.reply_file_lists);
    });
  }

  addReply() {
    this.router.navigate(['/home/forum/createReply'], {queryParams: {post_id: this.post_id, post_title: this.post.post_title}});
  }

  back() {
    this.location.back();
  }

  doubleBack() {
    this.location.back();
    this.location.back();
  }

}
