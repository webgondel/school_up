import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../../../services/api.service";
import {MatDialog} from "@angular/material/dialog";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {AddSubTopicDialogComponent} from "../sub-topic/add-sub-topic-dialog/add-sub-topic-dialog.component";
import {Location} from "@angular/common";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  sub_topic_id;
  sub_topic_title;
  subject_title;
  posts;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private router: Router,
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private location: Location
  ) { }

  ngOnInit() {
    this.sub_topic_id = this.route.snapshot.queryParamMap.get('sub_topic_id');
    this.sub_topic_title = this.route.snapshot.queryParamMap.get('sub_topic_title');
    this.subject_title = this.route.snapshot.queryParamMap.get('subject_title');
    this.getPostsBySubTopicId();
  }

  getPostsBySubTopicId() {
    this.apiService.getPostsBySubTopicId(this.sub_topic_id).subscribe(res => {
      this.posts = res;
      console.log(res);
    });
  }

  handleClickPost(post_id) {
    this.apiService.increaseViewsByPostId(post_id).subscribe();
    this.router.navigate(['/home/forum/postDetail'], {queryParams: {post_id, subject_title: this.subject_title, sub_topic_title: this.sub_topic_title}});
  }

  addPost() {
    this.router.navigate(['/home/forum/createPost'], {queryParams: {sub_topic_id: this.sub_topic_id}});
  }

  back() {
    this.location.back();
  }

}
