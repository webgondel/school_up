import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FormControl, Validators} from "@angular/forms";
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {ApiService} from "../../../../services/api.service";
import {AuthService} from "../../../../services/auth.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {

  sub_topic_id;
  currentUser;
  files: any = [];
  htmlContent;
  titleFormControl = new FormControl('', [
    Validators.required,
  ]);
  editorConfig: AngularEditorConfig = {
    editable: true,
    height: 'auto',
    minHeight: '300px',
  };

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private authService: AuthService,
    private location: Location
  ) { }

  ngOnInit() {
    this.sub_topic_id = this.route.snapshot.queryParamMap.get('sub_topic_id');
    this.currentUser = this.authService.currentUserValue;
  }

  uploadFile(files) {
    // console.log(event.target.files);
    // const files = (event.target as HTMLInputElement).files;
    for (let index = 0; index < files.length; index++) {
      this.files.push(files[index]);
    }
  }

  deleteAttachment(index) {
    this.files.splice(index, 1)
  }

  createPost() {
    if(this.titleFormControl.invalid || this.htmlContent === undefined || this.htmlContent === '') {
      alert('Please fill out title and content.');
      return;
    }
    const formData = new FormData();
    formData.append('post_title', this.titleFormControl.value);
    formData.append('post_content', this.htmlContent);
    formData.append('sub_topic_id', this.sub_topic_id);
    formData.append('user_id', this.currentUser.user.user_id);
    this.files.forEach(file => {
      formData.append('file[]', file);
    });
    console.log('formData>>>>>>>>>', typeof formData.get('file[]'));
    this.apiService.addPost(formData).subscribe(res => {
      this.location.back();
    });

  }

  back() {
    this.location.back();
  }

}
