import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {AngularEditorConfig} from "@kolkov/angular-editor";
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "../../../../services/api.service";
import {AuthService} from "../../../../services/auth.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-create-reply',
  templateUrl: './create-reply.component.html',
  styleUrls: ['./create-reply.component.scss']
})
export class CreateReplyComponent implements OnInit {

  post_id;
  post_title;
  currentUser;
  files: any = [];
  htmlContent;
  editorConfig: AngularEditorConfig = {
    editable: true,
    height: 'auto',
    minHeight: '300px',
  };

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private authService: AuthService,
    private location: Location
  ) { }

  ngOnInit() {
    this.post_id = this.route.snapshot.queryParamMap.get('post_id');
    this.post_title = this.route.snapshot.queryParamMap.get('post_title');
    this.currentUser = this.authService.currentUserValue;
    console.log(this.post_id);
  }

  uploadFile(files) {
    for (let index = 0; index < files.length; index++) {
      this.files.push(files[index]);
    }
  }

  deleteAttachment(index) {
    this.files.splice(index, 1)
  }

  createReply() {
    if(this.htmlContent === undefined || this.htmlContent === '') {
      alert('Please fill out title and content.');
      return;
    }
    const formData = new FormData();
    formData.append('reply_content', this.htmlContent);
    formData.append('post_id', this.post_id);
    formData.append('user_id', this.currentUser.user.user_id);
    this.files.forEach(file => {
      formData.append('file[]', file);
    });
    console.log('formData>>>>>>>>>', typeof formData.get('file[]'));
    this.apiService.addReply(formData).subscribe(res => {
      this.location.back();
    });

  }

  back() {
    this.location.back();
  }

}
