import { Component, OnInit } from '@angular/core';
import {AddNicknameDialogComponent} from "../../home/add-nickname-dialog/add-nickname-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {AddSubjectDialogComponent} from "./add-subject-dialog/add-subject-dialog.component";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ApiService} from "../../../services/api.service";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {

  subjects;
  currentUser;

  constructor(
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private apiService: ApiService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.getSubjects();
    this.currentUser = this.authService.currentUserValue;
  }

  addSubject() {
    const dialogRef = this.dialog.open(AddSubjectDialogComponent, {
      width: '500px',
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getSubjects();
      }
    });
  }

  getSubjects() {
    this.apiService.getSubjects().subscribe(res => {
      this.subjects = res;
    })
  }

  handleClickSubject(subject_id, subject_title) {
    this.apiService.increaseViewsBySubjectId(subject_id).subscribe();
    this.router.navigate(['/home/forum/subTopic'], {queryParams: {subject_id, subject_title}})
  }

}
