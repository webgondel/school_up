import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {ApiService} from "../../../../services/api.service";
import {AuthService} from "../../../../services/auth.service";

@Component({
  selector: 'app-add-subject-dialog',
  templateUrl: './add-subject-dialog.component.html',
  styleUrls: ['./add-subject-dialog.component.scss']
})
export class AddSubjectDialogComponent implements OnInit {

  currentUser;

  myFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(
    public dialogRef: MatDialogRef<AddSubjectDialogComponent>,
    private apiService: ApiService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addValue() {
    if (this.myFormControl.invalid) {
      return;
    }
    const value = this.myFormControl.value;
    this.apiService.addSubject(value).subscribe(res => {
      this.dialogRef.close(true);
    });
  }
}
