import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../../../services/api.service";
import {AddSubjectDialogComponent} from "../subject/add-subject-dialog/add-subject-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {AddSubTopicDialogComponent} from "./add-sub-topic-dialog/add-sub-topic-dialog.component";
import {Location} from "@angular/common";

@Component({
  selector: 'app-sub-topic',
  templateUrl: './sub-topic.component.html',
  styleUrls: ['./sub-topic.component.scss']
})
export class SubTopicComponent implements OnInit {

  subject_id;
  subject_title;
  sub_topics;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private router: Router,
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private location: Location
  ) { }

  ngOnInit() {
    this.subject_id = this.route.snapshot.queryParamMap.get('subject_id');
    this.subject_title = this.route.snapshot.queryParamMap.get('subject_title');
    this.getSubTopicsBySubjectId();
  }

  getSubTopicsBySubjectId() {
    this.apiService.getSubTopicsBySubjectId(this.subject_id).subscribe(res => {
      this.sub_topics = res;
      console.log(res);
    });
  }

  handleClickSubTopic(sub_topic_id, sub_topic_title) {
    this.apiService.increaseViewsBySubTopicId(sub_topic_id).subscribe();
    this.router.navigate(['/home/forum/post'], {queryParams: {sub_topic_id, sub_topic_title, subject_title: this.subject_title}})
  }

  addSubTopic() {
    const dialogRef = this.dialog.open(AddSubTopicDialogComponent, {
      width: '500px',
      data: {subject_id: this.subject_id}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getSubTopicsBySubjectId();
      }
    });
  }

  back() {
    this.location.back();
  }

}
