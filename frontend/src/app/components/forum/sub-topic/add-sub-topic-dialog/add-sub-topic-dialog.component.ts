import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ApiService} from "../../../../services/api.service";
import {AuthService} from "../../../../services/auth.service";

@Component({
  selector: 'app-add-sub-topic-dialog',
  templateUrl: './add-sub-topic-dialog.component.html',
  styleUrls: ['./add-sub-topic-dialog.component.scss']
})
export class AddSubTopicDialogComponent implements OnInit {

  currentUser;

  myFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(
    public dialogRef: MatDialogRef<AddSubTopicDialogComponent>,
    private apiService: ApiService,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addValue() {
    if (this.myFormControl.invalid) {
      return;
    }
    const value = this.myFormControl.value;
    this.apiService.addSubTopic(value, this.currentUser.user.user_id, this.data.subject_id).subscribe(res => {
      this.dialogRef.close(true);
    });
  }

}
