import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {MatDialog} from '@angular/material/dialog';
import {AddNicknameDialogComponent} from './add-nickname-dialog/add-nickname-dialog.component';
import {Router} from "@angular/router";
import {MatToolbarModule} from '@angular/material/toolbar';
import {SecurityQuestionDialogComponent} from "./security-question-dialog/security-question-dialog.component";
import {SnackBarNotification} from "../../utils/snack-bar-notification";
import {ChatService} from "../dragon-chat/services/chat.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  currentUser;
  navLinks: any[];
  activeLinkIndex = -1;

  constructor(
    private authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
    private snackBarNotification: SnackBarNotification,
    private chatService: ChatService
  ) {
    this.navLinks = [
      {
        label: 'Forum',
        link: '/home/forum',
        index: 0
      },
      {
        label: 'Live Klassenzimmer',
        link: '/home/video',
        index: 1
      },
      {
        label: 'Stundenplan',
        link: '/home/calendar',
        index: 2
      },
      {
        label: 'Cloud',
        link: '/home/cloud',
        index: 3
      },
      {
        label: 'UserAdmin',
        link: '/home/user',
        index: 4
      },
      {
        label: 'Chat',
        link: '/home/text-chat',
        index: 5
      },
      // {
      //   label: 'Video Konferenz',
      //   link: '/home/jitsi-meet',
      //   index: 6
      // },
    ];
  }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
    this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
    });
    if (this.currentUser.user.nickname === null || this.currentUser.user.nickname === '') {
      this.openAddNickNameDialog();
    }
    this.setChatSocket()
  }

  openAddNickNameDialog() {
    const dialogRef = this.dialog.open(AddNicknameDialogComponent, {
      width: '500px',
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.openSecurityQuestionDialog();
      } else {
        this.openAddNickNameDialog();
      }
    });
  }

  openSecurityQuestionDialog() {
    const dialogRef = this.dialog.open(SecurityQuestionDialogComponent, {
      width: '800px',
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.welcome);
      } else {
        this.openSecurityQuestionDialog();
      }
    });
  }

  setChatSocket() {
    this.chatService.setChatRoom();
  }

}
