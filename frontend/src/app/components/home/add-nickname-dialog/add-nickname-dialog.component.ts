import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import {ApiService} from '../../../services/api.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-add-nickname-dialog',
  templateUrl: './add-nickname-dialog.component.html',
  styleUrls: ['./add-nickname-dialog.component.scss']
})
export class AddNicknameDialogComponent implements OnInit {

  currentUser;

  nicknameFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(
    public dialogRef: MatDialogRef<AddNicknameDialogComponent>,
    private apiService: ApiService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
  }

  addNickName() {
    if (this.nicknameFormControl.invalid) {
      return;
    }
    const nickname = this.nicknameFormControl.value;
    this.apiService.addNickNameByUserId(nickname, this.currentUser.user.user_id).subscribe(res => {
      this.updateCurrentUser(nickname);
      this.dialogRef.close(true);
    });
  }

  updateCurrentUser(nickname) {
    let data = this.authService.currentUserValue;
    data.user.nickname = nickname;
    this.authService.updateCurrentUser(data);
  }
}

//   user_id: user.user_id,
//   first_name: user.first_name,
//   last_name: user.last_name,
//   nickname: user.nickname,
//   class: user.class,
//   role: user.role
