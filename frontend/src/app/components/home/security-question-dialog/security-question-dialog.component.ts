import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../../../services/api.service";
import {AuthService} from "../../../services/auth.service";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-security-question-dialog',
  templateUrl: './security-question-dialog.component.html',
  styleUrls: ['./security-question-dialog.component.scss']
})
export class SecurityQuestionDialogComponent implements OnInit {

  securityQuestions;
  securityForm: FormGroup;
  submitted = false;
  formData: any;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private authService: AuthService,
    private dialogRef: MatDialogRef<SecurityQuestionDialogComponent>
  ) { }

  ngOnInit(): void {
    this.securityForm = this.formBuilder.group({
      firstForm: ['', Validators.required],
      secondForm: ['', Validators.required],
      thirdForm: ['', Validators.required],
    });
  }

  get f() { return this.securityForm.controls; }

  confirm() {
    this.submitted = true;
    if(this.securityForm.invalid) {
      return
    }
    this.formData = {
      security_answer_1: this.f.firstForm.value,
      security_answer_2: this.f.secondForm.value,
      security_answer_3: this.f.thirdForm.value,
      user_id: this.authService.currentUserValue.user.user_id
    };
    this.apiService.addSecurityAnswers(this.formData).subscribe(res => {
      this.dialogRef.close(true);
    });
  }

  getSecurityQuestions() {
    this.apiService.getSecurityQuestions().subscribe(res => {
      this.securityQuestions = res;
    })
  }

}
