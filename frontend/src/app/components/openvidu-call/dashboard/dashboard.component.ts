import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService} from "../../../services/auth.service";
import {ApiService} from "../../../services/api.service";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ConfirmDialogBoxComponent} from "../../common/confirm-dialog-box/confirm-dialog-box.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  public roomForm: FormGroup;
  public version = require('../../../../../package.json').version;
  currentUser;
  public chatRooms = [];

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    private authService: AuthService,
    private apiService: ApiService,
    private snackBarNotification: SnackBarNotification,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
    this.roomForm = this.formBuilder.group({
      roomName: ['', Validators.compose([Validators.required])],
    });
    this.getChatRooms();
  }

  public addRoom() {
    if (this.roomForm.valid) {
      // const roomName = this.roomForm.value.roomName.replace(/ /g, '-');
      const roomName = this.roomForm.value.roomName;
      this.apiService.addChatRoom({room_name: roomName, class_name: this.currentUser.user.class}).subscribe(res => {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getChatRooms();
      })
      // this.router.navigate(['/home/video', roomName]);
    }
  }

  deleteChatRoomById(e, id) {
    e.stopPropagation();
    this.confirmDialog(id);
  }

  joinRoom(id) {
    this.router.navigate(['/home/video', id]);
  }

  getChatRooms() {
    this.apiService.getChatRoomsByClass(this.currentUser.user.class).subscribe((res: any) => {
      this.chatRooms = res;
    })
  }

  confirmDialog(id): void {
    const dialogRef = this.dialog.open(ConfirmDialogBoxComponent, {
      width: '550px',
      data: {alarmMessage: 'Are you sure you want to delete this room?'}
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if(confirm) {
        this.apiService.deleteChatRoomById(id).subscribe(res => {
          this.snackBarNotification.message(this.snackBarNotification.content.deleted);
          this.getChatRooms();
        });
      }
    });
  }

  goToRoomCloud(room_id) {
    this.router.navigate(['/home/video/room/cloud'], {queryParams: {room_id}})
  }

}
