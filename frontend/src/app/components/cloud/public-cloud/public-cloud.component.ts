import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {ApiService} from "../../../services/api.service";
import {Router} from "@angular/router";
import { AuthService } from 'src/app/services/auth.service';
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {AddFolderComponent} from "../add-folder/add-folder.component";
import {CloudDialogComponent} from "../cloud-dialog/cloud-dialog.component";
import {CloudPreviewDialogComponent} from "../cloud-preview-dialog/cloud-preview-dialog.component";

@Component({
  selector: 'app-public-cloud',
  templateUrl: './public-cloud.component.html',
  styleUrls: ['./public-cloud.component.scss']
})
export class PublicCloudComponent implements OnInit {

  folder_lists;
  file_lists;

  constructor(
    private location: Location,
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private apiService: ApiService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getPublicFolders();
    this.getPublicRootFiles();
  }

  back() {
    this.location.back();
  }

  addFolder() {
    const dialogRef = this.dialog.open(AddFolderComponent, {
      width: '800px',
      data: {
        status: 'public'
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getPublicFolders();
      }
    });
  }

  addFile() {
    const dialogRef = this.dialog.open(CloudDialogComponent, {
      width: '800px',
      data: {
        status: 'public',
        folder_id: 0
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getPublicRootFiles();
      }
    });
  }

  getPublicFolders() {
    this.apiService.getPublicFolders().subscribe(res => {
      this.folder_lists = res;
    })
  }

  getPublicRootFiles() {
    this.apiService.getPublicRootFiles().subscribe(res => {
      this.file_lists = res;
    })
  }

  goToCloudHome() {
    this.router.navigate(['/home/cloud']);
  }

}
