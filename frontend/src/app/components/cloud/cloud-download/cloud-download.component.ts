import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../../../services/api.service";
import { FileSaverService } from 'ngx-filesaver';

@Component({
  selector: 'app-cloud-download',
  templateUrl: './cloud-download.component.html',
  styleUrls: ['./cloud-download.component.scss']
})
export class CloudDownloadComponent implements OnInit {

  cloud;
  cloud_id;
  // downloadUrl = environment.apiUrl + 'upload';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private apiService: ApiService,
    private fileSaverService: FileSaverService
  ) { }

  ngOnInit(): void {
    this.cloud_id = this.route.snapshot.queryParamMap.get('cloud_id');
    this.getCloudByCloudId();
  }

  goToCloudHome() {
    this.router.navigate(['/home/cloud']);
  }

  back() {
    this.location.back();
  }

  getCloudByCloudId() {
    this.cloud = this.apiService.getCloudByCloudId(this.cloud_id).subscribe(res => {
      this.cloud = res[0];
    });
  }

  downloadCloud() {
    this.apiService.downloadCloud(this.cloud.file_name).subscribe(res => this.downloadFile(res.body));
  }

  downloadFile(data) {
    const fileType = this.fileSaverService.genType(this.cloud.origin_name);
    const blob = new Blob([data], {type: fileType});
    this.fileSaverService.save(blob, this.cloud.origin_name);
  }

}
