import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ApiService} from "../../../services/api.service";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-add-folder',
  templateUrl: './add-folder.component.html',
  styleUrls: ['./add-folder.component.scss']
})
export class AddFolderComponent implements OnInit {

  currentUser;

  myFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(
    public dialogRef: MatDialogRef<AddFolderComponent>,
    private apiService: ApiService,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addValue() {
    if (this.myFormControl.invalid) {
      return;
    }
    const value = this.myFormControl.value;
    this.apiService.addFolder({folder_name: value, user_id: this.currentUser.user.user_id, status: this.data.status, room_id: (this.data.room_id || '0')}).subscribe(res => {
      this.dialogRef.close(true);
    });
  }

}
