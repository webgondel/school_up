import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ApiService} from "../../../services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {AddFolderComponent} from "../add-folder/add-folder.component";
import {CloudDialogComponent} from "../cloud-dialog/cloud-dialog.component";

@Component({
  selector: 'app-public-cloud-folder',
  templateUrl: './public-cloud-folder.component.html',
  styleUrls: ['./public-cloud-folder.component.scss']
})
export class PublicCloudFolderComponent implements OnInit {

  file_lists;
  folder_id;
  folder_name = '';
  tooltipInfo;
  defaultTooltipInfo = 'share';
  copiedTooltipInfo = 'copied';
  folderInfo;

  constructor(
    private location: Location,
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private apiService: ApiService,
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.folder_id = this.route.snapshot.queryParamMap.get('folder_id');
    this.getFilesByFolderId();
    this.tooltipInfo = this.defaultTooltipInfo;
    this.getFolderByFolderId();
  }

  back() {
    this.location.back();
  }

  addFile() {
    const dialogRef = this.dialog.open(CloudDialogComponent, {
      width: '800px',
      data: {
        status: 'public',
        folder_id: this.folder_id
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getFilesByFolderId();
      }
    });
  }

  getFilesByFolderId() {
    this.apiService.getFilesByFolderId(this.folder_id).subscribe(res => {
      this.file_lists = res;
    })
  }

  goToCloudHome() {
    this.router.navigate(['/home/cloud']);
  }

  getFolderByFolderId() {
    this.apiService.getFolderByFolderId(this.folder_id).subscribe(res => {
      this.folderInfo = res[0];
      this.folder_name = this.folderInfo.folder_name;
    });
  }

}
