import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ApiService} from "../../../services/api.service";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {AddFolderComponent} from "../add-folder/add-folder.component";
import {CloudDialogComponent} from "../cloud-dialog/cloud-dialog.component";

@Component({
  selector: 'app-own-cloud',
  templateUrl: './own-cloud.component.html',
  styleUrls: ['./own-cloud.component.scss']
})
export class OwnCloudComponent implements OnInit {

  folder_lists;
  file_lists;
  currentUser;

  constructor(
    private location: Location,
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private apiService: ApiService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.currentUser = this.authService.currentUserValue;
    this.getPrivateFoldersByUserId();
    this.getPrivateRootFilesByUserId();
  }

  addFolder() {
    const dialogRef = this.dialog.open(AddFolderComponent, {
      width: '800px',
      data: {
        status: 'private',
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getPrivateFoldersByUserId();
      }
    });
  }

  addFile() {
    const dialogRef = this.dialog.open(CloudDialogComponent, {
      width: '800px',
      data: {
        status: 'private',
        folder_id: 0
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getPrivateRootFilesByUserId();
      }
    });
  }

  getPrivateFoldersByUserId() {
    this.apiService.getPrivateFoldersByUserId(this.currentUser.user.user_id).subscribe(res => {
      this.folder_lists = res;
    })
  }

  getPrivateRootFilesByUserId() {
    this.apiService.getPrivateRootFilesByUserId(this.currentUser.user.user_id).subscribe(res => {
      this.file_lists = res;
    })
  }

  goToCloudHome() {
    this.router.navigate(['/home/cloud']);
  }

  back() {
    this.location.back();
  }

}
