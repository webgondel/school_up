import { Component, OnInit } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {MatDialog} from "@angular/material/dialog";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ApiService} from "../../../services/api.service";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {CloudDialogComponent} from "../cloud-dialog/cloud-dialog.component";

@Component({
  selector: 'app-cloud-detail',
  templateUrl: './cloud-detail.component.html',
  styleUrls: ['./cloud-detail.component.scss']
})
export class CloudDetailComponent implements OnInit {

  cloud_lists;
  currentUser;
  uploadUrl = environment.apiUrl + 'upload/';

  constructor(
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private apiService: ApiService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.getClouds();
    this.currentUser = this.authService.currentUserValue;
  }

  addCloud() {
    const dialogRef = this.dialog.open(CloudDialogComponent, {
      width: '800px',
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getClouds();
      }
    });
  }

  getClouds() {
    this.apiService.getClouds().subscribe(res => {
      this.cloud_lists = res;
    })
  }

}
