import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-share-type-dialog',
  templateUrl: './share-type-dialog.component.html',
  styleUrls: ['./share-type-dialog.component.scss']
})
export class ShareTypeDialogComponent implements OnInit {

  selectedItem: string;
  selections = [
    {
      title: 'Per Email an Klassenmitglied senden', value: 'sendEmailToClassMember'
    },
    {
      title: 'Per Email an Person außerhalb meiner Klasse senden', value: 'sendEmailToOutsideMember'
    },
    {
      title: 'Link kopieren, um an Klassenmitglied zu senden', value: 'copyLinkToClassMember'
    },
    {
      title: 'Link kopieren, um Person außerhalb meiner Klasse zu senden', value: 'copyLinkToOutsideMember'
    },
  ];

  constructor(
    private dialogRef: MatDialogRef<ShareTypeDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirm() {
    if (!this.selectedItem) {
      return
    }
    this.dialogRef.close(this.selectedItem);
  }

}
