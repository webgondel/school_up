import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-cloud-home',
  templateUrl: './cloud-home.component.html',
  styleUrls: ['./cloud-home.component.scss']
})
export class CloudHomeComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {

  }

  goToMyCloud() {
    this.router.navigate(['/home/cloud/own-cloud'])
  }

  goToPublicCloud() {
    this.router.navigate(['/home/cloud/public-cloud'])
  }

}
