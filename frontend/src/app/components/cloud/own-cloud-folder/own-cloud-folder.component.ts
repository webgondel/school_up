import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ApiService} from "../../../services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {CloudDialogComponent} from "../cloud-dialog/cloud-dialog.component";

@Component({
  selector: 'app-own-cloud-folder',
  templateUrl: './own-cloud-folder.component.html',
  styleUrls: ['./own-cloud-folder.component.scss']
})
export class OwnCloudFolderComponent implements OnInit {

  file_lists;
  folder_id;
  folder_name = '';
  tooltipInfo;
  defaultTooltipInfo = 'share';
  copiedTooltipInfo = 'copied';
  currentUser;
  folderInfo: any;

  constructor(
    private location: Location,
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private apiService: ApiService,
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.currentUser = this.authService.currentUserValue;
    this.folder_id = this.route.snapshot.queryParamMap.get('folder_id');
    this.getFolderByFolderId();
    this.getPrivateFiles();
    this.tooltipInfo = this.defaultTooltipInfo;
  }

  back() {
    this.location.back();
  }

  addFile() {
    const dialogRef = this.dialog.open(CloudDialogComponent, {
      width: '800px',
      data: {
        status: 'private',
        folder_id: this.folder_id
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getPrivateFiles();
      }
    });
  }

  getPrivateFiles() {
    this.apiService.getPrivateFiles({folder_id: this.folder_id, user_id: this.currentUser.user.user_id}).subscribe(res => {
      this.file_lists = res;
    })
  }

  goToCloudHome() {
    this.router.navigate(['/home/cloud']);
  }

  getFolderByFolderId() {
    this.apiService.getFolderByFolderId(this.folder_id).subscribe(res => {
      this.folderInfo = res[0];
      this.folder_name = this.folderInfo.folder_name;
    });
  }

}
