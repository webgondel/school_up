import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ApiService} from "../../../services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {AddFolderComponent} from "../add-folder/add-folder.component";
import {CloudDialogComponent} from "../cloud-dialog/cloud-dialog.component";

@Component({
  selector: 'app-room-cloud',
  templateUrl: './room-cloud.component.html',
  styleUrls: ['./room-cloud.component.scss']
})
export class RoomCloudComponent implements OnInit {

  folder_lists;
  file_lists;
  room_id;

  constructor(
    private location: Location,
    public dialog: MatDialog,
    private snackBarNotification: SnackBarNotification,
    private apiService: ApiService,
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.room_id = this.route.snapshot.queryParamMap.get('room_id');
    console.log('=======>', this.room_id);
    this.getRoomFoldersById();
    this.getRoomRootFilesById();
  }

  back() {
    this.location.back();
  }

  addFolder() {
    const dialogRef = this.dialog.open(AddFolderComponent, {
      width: '800px',
      data: {
        status: 'room',
        room_id: this.room_id
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getRoomFoldersById();
      }
    });
  }

  addFile() {
    const dialogRef = this.dialog.open(CloudDialogComponent, {
      width: '800px',
      data: {
        status: 'room',
        folder_id: 0,
        room_id: this.room_id
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.created);
        this.getRoomRootFilesById();
      }
    });
  }

  getRoomFoldersById() {
    this.apiService.getRoomFoldersById(this.room_id).subscribe(res => {
      this.folder_lists = res;
    })
  }

  getRoomRootFilesById() {
    this.apiService.getRoomRootFilesById(this.room_id).subscribe(res => {
      this.file_lists = res;
    })
  }

  goToCloudHome() {
    this.router.navigate(['/home/cloud']);
  }

}
