import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {AngularEditorConfig} from "@kolkov/angular-editor";
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "../../../services/api.service";
import {AuthService} from "../../../services/auth.service";
import {Location} from "@angular/common";
import {MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-cloud-dialog',
  templateUrl: './cloud-dialog.component.html',
  styleUrls: ['./cloud-dialog.component.scss']
})
export class CloudDialogComponent implements OnInit {

  currentUser;
  files: any = [];

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private authService: AuthService,
    private location: Location,
    public dialogRef: MatDialogRef<CloudDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
  }

  uploadFile(files) {
    for (let index = 0; index < files.length; index++) {
      this.files.push(files[index]);
    }
  }

  deleteAttachment(index) {
    this.files.splice(index, 1)
  }

  uploadCloud() {
    if (this.files.length === 0) {
      return;
    }
    const formData = new FormData();
    formData.append('user_id', this.currentUser.user.user_id);
    formData.append('status', this.data.status);
    formData.append('folder_id', this.data.folder_id);
    formData.append('room_id', (this.data.room_id || '0'));
    this.files.forEach(file => {
      formData.append('file[]', file);
    });
    this.apiService.addCloud(formData).subscribe(res => {
      this.dialogRef.close(true);
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
