import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../../../services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-global-public-file-lists',
  templateUrl: './global-public-file-lists.component.html',
  styleUrls: ['./global-public-file-lists.component.scss']
})
export class GlobalPublicFileListsComponent implements OnInit {

  file_lists;
  folder_id;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.folder_id = this.route.snapshot.queryParamMap.get('folder_id');
    this.getFilesByFolderId();
  }

  getFilesByFolderId() {
    this.apiService.getFilesByFolderId(this.folder_id).subscribe(res => {
      this.file_lists = res;
    })
  }

  goToCloudHome() {
    this.router.navigate(['/home/cloud']);
  }

  back() {
    this.location.back();
  }

}
