import { Component, OnInit } from '@angular/core';
import {privateDecrypt} from "crypto";
import {Router} from "@angular/router";

@Component({
  selector: 'app-global-cloud',
  templateUrl: './global-cloud.component.html',
  styleUrls: ['./global-cloud.component.scss']
})
export class GlobalCloudComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

}
