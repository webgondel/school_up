import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-global-public-file-download',
  templateUrl: './global-public-file-download.component.html',
  styleUrls: ['./global-public-file-download.component.scss']
})
export class GlobalPublicFileDownloadComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
