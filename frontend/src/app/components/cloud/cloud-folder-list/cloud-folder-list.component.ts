import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {FormControl, Validators} from "@angular/forms";
import {ApiService} from "../../../services/api.service";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ConfirmDialogBoxComponent} from "../../common/confirm-dialog-box/confirm-dialog-box.component";
import {MatDialog} from "@angular/material/dialog";
import {SendEmailDialogComponent} from "../../common/send-email-dialog/send-email-dialog.component";
import {ShareTypeDialogComponent} from "../share-type-dialog/share-type-dialog.component";

@Component({
  selector: 'app-cloud-folder-list',
  templateUrl: './cloud-folder-list.component.html',
  styleUrls: ['./cloud-folder-list.component.scss']
})
export class CloudFolderListComponent implements OnInit {

  tooltipInfo;
  defaultTooltipInfo = 'share';
  copiedTooltipInfo = 'copied';
  @Input() folder_lists;
  @Input() cloud_type;
  isEditing = [];
  textField;

  constructor(
    private router: Router,
    public authService: AuthService,
    private apiService: ApiService,
    private snackBarNotification: SnackBarNotification,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.tooltipInfo = this.defaultTooltipInfo;
  }

  goToCloudFolder(folder_id) {
    if (this.cloud_type === 'public') {
      this.router.navigate(['/home/cloud/public-cloud-folder'], {queryParams: {folder_id}});
    }
    if (this.cloud_type === 'private') {
      this.router.navigate(['/home/cloud/own-cloud-folder'], {queryParams: {folder_id}});
    }
    if (this.cloud_type === 'room') {
      this.router.navigate(['/home/video/room/cloud-folder'], {queryParams: {folder_id}});
    }
  }

  shareFolder(e, folder_id) {
    e.stopPropagation();

    const dialogRef = this.dialog.open(ShareTypeDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        let copyText;

        if (res === 'sendEmailToClassMember') {
          if (this.cloud_type === 'public') {
            copyText = 'https://' + window.location.host + `/home/cloud/public-cloud-folder?folder_id=${folder_id}`;
          }
          if (this.cloud_type === 'private') {
            copyText = 'https://' + window.location.host + `/home/cloud/own-cloud-folder?folder_id=${folder_id}`;
          }
          this.openSendEmailDialog(copyText);
        }

        if (res === 'sendEmailToOutsideMember') {
          copyText = 'https://' + window.location.host + `/global-cloud/global-file-lists?folder_id=${folder_id}`;
          this.openSendEmailDialog(copyText);
        }

        if (res === 'copyLinkToClassMember') {
          if (this.cloud_type === 'public') {
            copyText = 'https://' + window.location.host + `/home/cloud/public-cloud-folder?folder_id=${folder_id}`;
          }
          if (this.cloud_type === 'private') {
            copyText = 'https://' + window.location.host + `/home/cloud/own-cloud-folder?folder_id=${folder_id}`;
          }
          this.copyContext(copyText);
        }

        if (res === 'copyLinkToOutsideMember') {
          copyText = 'https://' + window.location.host + `/global-cloud/global-file-lists?folder_id=${folder_id}`;
          this.copyContext(copyText);
        }
      }
    });
  }

  copyContext(copyText) {
    const textArea = document.createElement("textarea");
    textArea.value = copyText;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    this.snackBarNotification.message(this.snackBarNotification.content.copied);
  }

  editField(e, id) {
    e.stopPropagation();
    this.isEditing = [];
    this.isEditing[id] = true;
    const selectedField = this.folder_lists.find(item => item.folder_id === id);
    this.textField = selectedField.folder_name;
  }

  saveField(e, id) {
    e.stopPropagation();
    this.isEditing = [];
    this.apiService.updateFolderById(id, {folder_name: this.textField}).subscribe(res => {
      this.snackBarNotification.message(this.snackBarNotification.content.updated);
      const index = this.folder_lists.findIndex(item => item.folder_id === id);
      this.folder_lists[index].folder_name = this.textField;
    });
  }

  deleteField(e, id) {
    e.stopPropagation();
    this.confirmDialog(id);
  }

  cancelField(e) {
    e.stopPropagation();
    this.isEditing = [];
  }

  confirmDialog(id): void {
    const dialogRef = this.dialog.open(ConfirmDialogBoxComponent, {
      width: '500px',
      data: {alarmMessage: 'Sind Sie sich sicher, dass Sie diesen Ordner löschen möchten?'}
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if(confirm) {
        this.apiService.deleteFolderById(id).subscribe(res => {
          this.snackBarNotification.message(this.snackBarNotification.content.deleted);
          this.folder_lists = this.folder_lists.filter(item => item.folder_id !== id);
        });
      }
    });
  }

  openSendEmailDialog(sharableLink) {
    const dialogRef = this.dialog.open(SendEmailDialogComponent, {
      width: '500px',
      data: {content: sharableLink}
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if(confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.sent);
      } else {
        this.snackBarNotification.message(this.snackBarNotification.content.failed);
      }
    });
  }

}


