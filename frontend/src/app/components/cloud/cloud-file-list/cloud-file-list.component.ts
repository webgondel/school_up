import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {ApiService} from "../../../services/api.service";
import {SnackBarNotification} from "../../../utils/snack-bar-notification";
import {ConfirmDialogBoxComponent} from "../../common/confirm-dialog-box/confirm-dialog-box.component";
import {MatDialog} from "@angular/material/dialog";
import {ShareTypeDialogComponent} from "../share-type-dialog/share-type-dialog.component";
import {SendEmailDialogComponent} from "../../common/send-email-dialog/send-email-dialog.component";

@Component({
  selector: 'app-cloud-file-list',
  templateUrl: './cloud-file-list.component.html',
  styleUrls: ['./cloud-file-list.component.scss']
})
export class CloudFileListComponent implements OnInit {

  tooltipInfo;
  defaultTooltipInfo = 'share';
  copiedTooltipInfo = 'copied';
  @Input() file_lists;
  @Input() cloud_type;
  isEditing = [];
  textField;

  constructor(
    private router: Router,
    public authService: AuthService,
    private apiService: ApiService,
    private snackBarNotification: SnackBarNotification,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.tooltipInfo = this.defaultTooltipInfo;
  }

  shareCloud(e, cloud_id) {
    e.stopPropagation();

    const dialogRef = this.dialog.open(ShareTypeDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        let copyText;

        if (res === 'sendEmailToClassMember') {
          copyText = 'https://' + window.location.host + `/home/cloud/cloud-download?cloud_id=${cloud_id}`;
          this.openSendEmailDialog(copyText);
        }

        if (res === 'sendEmailToOutsideMember') {
          copyText = 'https://' + window.location.host + `/global-cloud/global-file-download?cloud_id=${cloud_id}`;
          this.openSendEmailDialog(copyText);
        }

        if (res === 'copyLinkToClassMember') {
          copyText = 'https://' + window.location.host + `/home/cloud/cloud-download?cloud_id=${cloud_id}`;
          this.copyContext(copyText);
        }

        if (res === 'copyLinkToOutsideMember') {
          copyText = 'https://' + window.location.host + `/global-cloud/global-file-download?cloud_id=${cloud_id}`;
          this.copyContext(copyText);
        }
      }
    });

  }

  copyContext(copyText) {
    const textArea = document.createElement("textarea");
    textArea.value = copyText;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    this.snackBarNotification.message(this.snackBarNotification.content.copied);
  }

  goToFileDownLoad(cloud_id) {
    if (this.cloud_type === 'global') {
      this.router.navigate(['/global-cloud/global-file-download'], {queryParams: {cloud_id}});
    } else {
      this.router.navigate(['/home/cloud/cloud-download'], {queryParams: {cloud_id}});
    }
  }

  editField(e, id) {
    e.stopPropagation();
    this.isEditing = [];
    this.isEditing[id] = true;
    const selectedField = this.file_lists.find(item => item.cloud_id === id);
    this.textField = selectedField.origin_name;
  }

  saveField(e, id) {
    e.stopPropagation();
    this.isEditing = [];
    this.apiService.updateCloudById(id, {origin_name: this.textField}).subscribe(res => {
      this.snackBarNotification.message(this.snackBarNotification.content.updated);
      const index = this.file_lists.findIndex(item => item.cloud_id === id);
      this.file_lists[index].origin_name = this.textField;
    });
  }

  deleteField(e, id) {
    e.stopPropagation();
    this.confirmDialog(id);
  }

  cancelField(e) {
    e.stopPropagation();
    this.isEditing = [];
  }

  confirmDialog(id): void {
    const dialogRef = this.dialog.open(ConfirmDialogBoxComponent, {
      width: '500px',
      data: {alarmMessage: 'Wollen Sie diese Datei wirklich unwiderruflich entfernen?'}
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if(confirm) {
        this.apiService.deleteCloudById(id).subscribe(res => {
          this.snackBarNotification.message(this.snackBarNotification.content.deleted);
          this.file_lists = this.file_lists.filter(item => item.cloud_id !== id);
        });
      }
    });
  }

  openSendEmailDialog(sharableLink) {
    const dialogRef = this.dialog.open(SendEmailDialogComponent, {
      width: '500px',
      data: {content: sharableLink}
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if(confirm) {
        this.snackBarNotification.message(this.snackBarNotification.content.sent);
      } else {
        this.snackBarNotification.message(this.snackBarNotification.content.failed);
      }
    });
  }

}
