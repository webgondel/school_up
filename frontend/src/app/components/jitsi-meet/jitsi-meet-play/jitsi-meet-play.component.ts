import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";

declare var JitsiMeetExternalAPI: any;

@Component({
  selector: 'app-jitsi-meet-play',
  templateUrl: './jitsi-meet-play.component.html',
  styleUrls: ['./jitsi-meet-play.component.scss']
})
export class JitsiMeetPlayComponent implements OnInit, AfterViewInit {

  roomId;
  options: any;
  jitsiAPI: any;

  constructor(
    public route: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.roomId = params.roomId;
    })
  }

  ngAfterViewInit(): void {
    this.options = {
      roomName: this.roomId,
      width: '100%',
      height: '100%',
      parentNode: document.querySelector('#meet')
    };
    this.jitsiAPI = new JitsiMeetExternalAPI('digischool24.de', this.options);
    this.jitsiAPI.on('readyToClose', () => {
      console.log('call hung up fron ON event');
      this.router.navigate(['/home/video']);
    });
  }

}
