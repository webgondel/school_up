import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
@Injectable()
export class SnackBarNotification {
  content = {
    updated: 'Erfolgreich geändert...',
    created: 'Erfolgreich erstellt...',
    deleted: 'Erfolgreich gelöscht...',
    sent: 'Erfolgreich gesendet...',
    failed: 'Scheitern...',
    copied: 'Erfolgreich kopiert',
    welcome: 'Herzlich willkommen'
  };
  constructor(public _snackBar: MatSnackBar) {}
  message(msg) {
    this._snackBar.open(msg, 'schließen', {
      duration: 3000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
    });
  }
}
