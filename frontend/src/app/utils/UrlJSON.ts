import {environment} from '../../environments/environment';

const baseUrl = environment.baseUrl;
const apiUrl = environment.apiUrl;

export const UrlJSON = {
  loginUrl: apiUrl + 'auth/login',
  signupUrl: apiUrl + 'auth/signup',
  resetRequestUrl: apiUrl + 'auth/resetRequest',
  resetPasswordUrl: apiUrl + 'auth/resetPassword',
  mainUrl: apiUrl + 'main',
  cloudUrl: apiUrl + 'upload',
  userUrl: apiUrl + 'user',
  chatUrl: apiUrl + 'chat',
};
