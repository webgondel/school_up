import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import {environment} from "../environments/environment";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import {MaterialModule} from './material.module';
import { AppRoutingModule } from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {JwtInterceptor} from './helper/jwt.interceptor';
import {SnackBarNotification} from './utils/snack-bar-notification';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import {MatDialogModule} from '@angular/material/dialog';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';

import { createCustomElement } from '@angular/elements';
import { ElementZoneStrategyFactory } from 'elements-zone-strategy';
import { NgxLinkifyjsModule } from 'ngx-linkifyjs';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FileSaverModule } from 'ngx-filesaver';
import { NgChatModule } from 'ng-chat';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: environment.baseUrl, options: {} };

// components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/auth/login/login.component';
import { MainLayoutComponent } from './components/layouts/main-layout/main-layout.component';
import { HomeComponent } from './components/home/home.component';
import { AddNicknameDialogComponent } from './components/home/add-nickname-dialog/add-nickname-dialog.component';
import { SubjectComponent } from './components/forum/subject/subject.component';
import { SubTopicComponent } from './components/forum/sub-topic/sub-topic.component';
import { PostComponent } from './components/forum/post/post.component';
import { AddSubjectDialogComponent } from './components/forum/subject/add-subject-dialog/add-subject-dialog.component';
import { ForumComponent } from './components/forum/forum.component';
import { AddSubTopicDialogComponent } from './components/forum/sub-topic/add-sub-topic-dialog/add-sub-topic-dialog.component';
import { PostDetailComponent } from './components/forum/post/post-detail/post-detail.component';
import { CreatePostComponent } from './components/forum/post/create-post/create-post.component';
import { DragDropDirective } from './directives/drag-drop.directive';
import { CreateReplyComponent } from './components/forum/post/create-reply/create-reply.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { UserAdminComponent } from './components/user-admin/user-admin.component';
import { ConfirmDialogBoxComponent } from './components/common/confirm-dialog-box/confirm-dialog-box.component';
import { AddUserDialogComponent } from './components/user-admin/add-user-dialog/add-user-dialog.component';
import { EditUserDialogComponent } from './components/user-admin/edit-user-dialog/edit-user-dialog.component';
import { CloudComponent } from './components/cloud/cloud.component';
import { CloudDialogComponent } from './components/cloud/cloud-dialog/cloud-dialog.component';
import {ApiService} from "./services/api.service";
import {AuthService} from "./services/auth.service";
import {UserService} from "./services/user.service";
import {AuthGuard} from "./guard/auth.guard";
import { OpenviduCallComponent } from './components/openvidu-call/openvidu-call.component';
import { VideoRoomComponent } from './components/openvidu-call/video-room/video-room.component';
import { DashboardComponent } from './components/openvidu-call/dashboard/dashboard.component';
import { OpenViduService } from './components/openvidu-call/shared/services/openvidu.service';
import { StreamComponent } from './components/openvidu-call/shared/components/stream/stream.component';
import { ChatComponent } from './components/openvidu-call/shared/components/chat/chat.component';
import { DialogExtensionComponent } from './components/openvidu-call/shared/components/dialog-extension/dialog-extension.component';
import { OpenViduVideoComponent } from './components/openvidu-call/shared/components/stream/ov-video.component';
import { DialogErrorComponent } from './components/openvidu-call/shared/components/dialog-error/dialog-error.component';
import { WebComponentComponent } from './components/openvidu-call/web-component/web-component.component';
import { ToolbarComponent } from './components/openvidu-call/shared/components/toolbar/toolbar.component';
import { DialogChooseRoomComponent } from './components/openvidu-call/shared/components/dialog-choose-room/dialog-choose-room.component';
import { FirstComponent } from './test/first/first.component';
import { SecondComponent } from './test/second/second.component';
import { CloudDetailComponent } from './components/cloud/cloud-detail/cloud-detail.component';
import { AddFolderComponent } from './components/cloud/add-folder/add-folder.component';
import { PublicCloudComponent } from './components/cloud/public-cloud/public-cloud.component';
import { CloudHomeComponent } from './components/cloud/cloud-home/cloud-home.component';
import { PublicCloudFolderComponent } from './components/cloud/public-cloud-folder/public-cloud-folder.component';
import { CloudPreviewDialogComponent } from './components/cloud/cloud-preview-dialog/cloud-preview-dialog.component';
import { CloudDownloadComponent } from './components/cloud/cloud-download/cloud-download.component';
import { OwnCloudComponent } from './components/cloud/own-cloud/own-cloud.component';
import { OwnCloudFolderComponent } from './components/cloud/own-cloud-folder/own-cloud-folder.component';
import { CloudFileListComponent } from './components/cloud/cloud-file-list/cloud-file-list.component';
import { CloudFolderListComponent } from './components/cloud/cloud-folder-list/cloud-folder-list.component';
import { GlobalPublicFileDownloadComponent } from './components/cloud/global-cloud/global-public-file-download/global-public-file-download.component';
import {GlobalPublicFileListsComponent} from "./components/cloud/global-cloud/global-public-file-lists/global-public-file-lists.component";
import { GlobalCloudComponent } from './components/cloud/global-cloud/global-cloud.component';
import { SendEmailDialogComponent } from './components/common/send-email-dialog/send-email-dialog.component';
import { AddMultiUsersDialogComponent } from './components/user-admin/add-multi-users-dialog/add-multi-users-dialog.component';
import { ShareTypeDialogComponent } from './components/cloud/share-type-dialog/share-type-dialog.component';
import { SecurityQuestionDialogComponent } from './components/home/security-question-dialog/security-question-dialog.component';
import { ConfirmUsernameDialogComponent } from './components/auth/confirm-username-dialog/confirm-username-dialog.component';
import { ConfirmSecurityDialogComponent } from './components/auth/confirm-security-dialog/confirm-security-dialog.component';
import { ResetPasswordDialogComponent } from './components/auth/reset-password-dialog/reset-password-dialog.component';
import { RoomCloudComponent } from './components/cloud/room-cloud/room-cloud.component';
import { RoomCloudFolderComponent } from './components/cloud/room-cloud-folder/room-cloud-folder.component';
import { DragonChatComponent } from './components/dragon-chat/dragon-chat.component';
import { UserListComponent } from './components/dragon-chat/user-list/user-list.component';
import { MessageBoxComponent } from './components/dragon-chat/message-box/message-box.component';
import { JitsiMeetComponent } from './components/jitsi-meet/jitsi-meet.component';
import { JitsiMeetPlayComponent } from './components/jitsi-meet/jitsi-meet-play/jitsi-meet-play.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainLayoutComponent,
    HomeComponent,
    AddNicknameDialogComponent,
    SubjectComponent,
    SubTopicComponent,
    PostComponent,
    AddSubjectDialogComponent,
    ForumComponent,
    AddSubTopicDialogComponent,
    PostDetailComponent,
    CreatePostComponent,
    DragDropDirective,
    CreateReplyComponent,
    CalendarComponent,
    UserAdminComponent,
    ConfirmDialogBoxComponent,
    AddUserDialogComponent,
    EditUserDialogComponent,
    CloudComponent,
    CloudDialogComponent,
    OpenviduCallComponent,
    VideoRoomComponent,
    DashboardComponent,
    StreamComponent,
    ChatComponent,
    DialogExtensionComponent,
    OpenViduVideoComponent,
    DialogErrorComponent,
    DialogChooseRoomComponent,
    WebComponentComponent,
    ToolbarComponent,
    FirstComponent,
    SecondComponent,
    CloudDetailComponent,
    AddFolderComponent,
    PublicCloudComponent,
    CloudHomeComponent,
    PublicCloudFolderComponent,
    CloudPreviewDialogComponent,
    CloudDownloadComponent,
    OwnCloudComponent,
    OwnCloudFolderComponent,
    CloudFileListComponent,
    CloudFolderListComponent,
    GlobalPublicFileDownloadComponent,
    GlobalPublicFileListsComponent,
    GlobalCloudComponent,
    SendEmailDialogComponent,
    AddMultiUsersDialogComponent,
    ShareTypeDialogComponent,
    SecurityQuestionDialogComponent,
    ConfirmUsernameDialogComponent,
    ConfirmSecurityDialogComponent,
    ResetPasswordDialogComponent,
    RoomCloudComponent,
    RoomCloudFolderComponent,
    DragonChatComponent,
    UserListComponent,
    MessageBoxComponent,
    JitsiMeetComponent,
    JitsiMeetPlayComponent,
  ],
  entryComponents: [
    AddNicknameDialogComponent,
    AddSubjectDialogComponent,
    AddSubTopicDialogComponent,
    AddUserDialogComponent,
    EditUserDialogComponent,
    ConfirmDialogBoxComponent,
    CloudDialogComponent,
    AddFolderComponent,
    CloudPreviewDialogComponent,
    SendEmailDialogComponent,
    AddMultiUsersDialogComponent,
    ShareTypeDialogComponent,
    SecurityQuestionDialogComponent,
    ConfirmSecurityDialogComponent,
    ConfirmUsernameDialogComponent,
    ResetPasswordDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SlickCarouselModule,
    MatDialogModule,
    AngularEditorModule,
    NgbModalModule,
    FlexLayoutModule,
    SocketIoModule.forRoot(config),
    FlatpickrModule.forRoot(),
    NgxLinkifyjsModule.forRoot(),
    FileSaverModule,
    NgChatModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    HttpClientModule,
    HttpClient,
    SnackBarNotification,
    ApiService,
    AuthService,
    UserService,
    AuthGuard,
    OpenViduService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private injector: Injector) {
    const strategyFactory = new ElementZoneStrategyFactory(WebComponentComponent, this.injector);
    const element = createCustomElement(WebComponentComponent, { injector: this.injector, strategyFactory });
    // @ts-ignore
    customElements.define('openvidu-webcomponent', element);
  }

  ngDoBootstrap() {}
}
