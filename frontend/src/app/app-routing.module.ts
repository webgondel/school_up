import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './guard/auth.guard';

import {LoginComponent} from './components/auth/login/login.component';
import {MainLayoutComponent} from './components/layouts/main-layout/main-layout.component';
import {HomeComponent} from './components/home/home.component';
import {ForumComponent} from "./components/forum/forum.component";
import {SubjectComponent} from "./components/forum/subject/subject.component";
import {SubTopicComponent} from "./components/forum/sub-topic/sub-topic.component";
import {PostComponent} from "./components/forum/post/post.component";
import {PostDetailComponent} from "./components/forum/post/post-detail/post-detail.component";
import {CreatePostComponent} from "./components/forum/post/create-post/create-post.component";
import {CreateReplyComponent} from "./components/forum/post/create-reply/create-reply.component";
import {OpenviduCallComponent} from "./components/openvidu-call/openvidu-call.component";
import { DashboardComponent } from './components/openvidu-call/dashboard/dashboard.component';
import { VideoRoomComponent } from './components/openvidu-call/video-room/video-room.component';
import {FirstComponent} from "./test/first/first.component";
import {SecondComponent} from "./test/second/second.component";
import {CalendarComponent} from "./components/calendar/calendar.component";
import {CloudComponent} from "./components/cloud/cloud.component";
import {UserAdminComponent} from "./components/user-admin/user-admin.component";
import {PublicCloudComponent} from "./components/cloud/public-cloud/public-cloud.component";
import {CloudHomeComponent} from "./components/cloud/cloud-home/cloud-home.component";
import {PublicCloudFolderComponent} from "./components/cloud/public-cloud-folder/public-cloud-folder.component";
import {CloudDownloadComponent} from "./components/cloud/cloud-download/cloud-download.component";
import {OwnCloudComponent} from "./components/cloud/own-cloud/own-cloud.component";
import {OwnCloudFolderComponent} from "./components/cloud/own-cloud-folder/own-cloud-folder.component";
import {GlobalPublicFileListsComponent} from "./components/cloud/global-cloud/global-public-file-lists/global-public-file-lists.component";
import {GlobalPublicFileDownloadComponent} from "./components/cloud/global-cloud/global-public-file-download/global-public-file-download.component";
import {GlobalCloudComponent} from "./components/cloud/global-cloud/global-cloud.component";
import {RoomCloudComponent} from "./components/cloud/room-cloud/room-cloud.component";
import {RoomCloudFolderComponent} from "./components/cloud/room-cloud-folder/room-cloud-folder.component";
import {DragonChatComponent} from "./components/dragon-chat/dragon-chat.component";
import {JitsiMeetComponent} from "./components/jitsi-meet/jitsi-meet.component";
import {JitsiMeetPlayComponent} from "./components/jitsi-meet/jitsi-meet-play/jitsi-meet-play.component";


const routes: Routes = [
  { path: 'first', component:  FirstComponent},
  { path: 'second', component:  SecondComponent},
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'home',
        canActivate: [AuthGuard],
        component: HomeComponent,
        children: [
          {
            path: '',
            redirectTo: 'forum',
            pathMatch: 'full',
          },
          {
            path: 'forum',
            component: ForumComponent,
            children: [
              {
                path: '',
                redirectTo: 'subject',
                pathMatch: 'full',
              },
              {
                path: 'subject',
                component: SubjectComponent
              },
              {
                path: 'subTopic',
                component: SubTopicComponent,
              },
              {
                path: 'post',
                component: PostComponent,
              },
              {
                path: 'postDetail',
                component: PostDetailComponent,
              },
              {
                path: 'createPost',
                component: CreatePostComponent,
              },
              {
                path: 'createReply',
                component: CreateReplyComponent,
              },
            ]
          },
          // {
          //   path: 'video',
          //   component: OpenviduCallComponent,
          //   canActivate: [AuthGuard],
          //   children: [
          //     {
          //       path: '',
          //       component: DashboardComponent,
          //       pathMatch: 'full',
          //     },
          //     {
          //       path: ':roomName',
          //       component: VideoRoomComponent
          //     },
          //     {
          //       path: 'room/cloud',
          //       component: RoomCloudComponent
          //     },
          //     {
          //       path: 'room/cloud-folder',
          //       component: RoomCloudFolderComponent
          //     }
          //   ]
          // },
          {
            path: 'calendar',
            component: CalendarComponent
          },
          {
            path: 'cloud',
            component: CloudComponent,
            children: [
              {
                path: '',
                component: CloudHomeComponent,
                pathMatch: 'full',
              },
              {
                path: 'public-cloud',
                component: PublicCloudComponent
              },
              {
                path: 'public-cloud-folder',
                component: PublicCloudFolderComponent
              },
              {
                path: 'own-cloud',
                component: OwnCloudComponent
              },
              {
                path: 'own-cloud-folder',
                component: OwnCloudFolderComponent
              },
              {
                path: 'cloud-download',
                component: CloudDownloadComponent
              },
            ]
          },
          {
            path: 'user',
            component: UserAdminComponent
          },
          {
            path: 'text-chat',
            component:DragonChatComponent
          },
          {
            path: 'video',
            component:JitsiMeetComponent
          },
        ]
      },
    ]
  },
  {
    path: 'global-cloud',
    component: GlobalCloudComponent,
    children: [
      {
        path: 'global-file-lists',
        component: GlobalPublicFileListsComponent
      },
      {
        path: 'global-file-download',
        component: GlobalPublicFileDownloadComponent
      },
    ]
  },
  {
    path: 'play/:roomId',
    component: JitsiMeetPlayComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
