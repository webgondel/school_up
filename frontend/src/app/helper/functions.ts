export const parseDate = (s) => {
  let d = new Date(s);
  return d.toLocaleString();
};
